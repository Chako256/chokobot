#-*- coding:utf-8 -*-

import re
import urllib.request
import html.parser
from bs4 import BeautifulSoup

from mybot import module

class Urls(module.BotModule):
	def __init__(self, name, client, priority=None):
		module.BotModule.__init__(self, name, client, priority=priority)

	def on_pubmsg(self, server, event):
		# Get all URLs (HTTP and HTTPS) in the line
		matches = re.findall(
			'(https?://[^\s]+)',
			event.arguments()[len(event.arguments()) - 1],
			flags=re.DOTALL | re.UNICODE
		)
		if matches is not None:
			for url in matches:
				title = self.getTitle(url)

				if title is not None:
					server.privmsg(
						event.target(),
						'[Title] {}'.format(title)
					)

	"""
	Get page title to a chan for a given URL

	Return the title content or None if an error occured
	"""
	def getTitle(self, url):
		try:
			req = urllib.request.Request(url)
			req.add_header('User-agent', 'Mozilla/5.0')

			response = urllib.request.urlopen(req, timeout=5)
			try:
				content_type = response.getheader('Content-Type')
				if not content_type.startswith('text/html'):
					return None
				response_charset = response.getheader('Content-Type').split(';')[1].split('=')[1]
			except:
				# Charset= does not exists in the Content-Type header
				# Using UTF-8 charset by default
				response_charset = 'UTF-8'
			content = str(response.read(), response_charset)

			soup = BeautifulSoup(content, "html5lib")
			if soup.title.string is not None and len(soup.title.string) > 0:
				return soup.title.string
		except Exception as e:
			self.error('[URLS] %s' % (str(e)))

		return None
