﻿#-*- coding:utf-8 -*-

# UNO Game Module for MyBot

import random
import os.path
from modules.common.game import game
from modules.common.game import util
from mybot import timer
from datetime import datetime

# Constants - Cards values and colors

CARDS = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+2', 'chg', 'noplay' ]
SPECIAL = [ '+4', 'joker' ]
COLORS = { 'Vert': 3, 'Rouge': 4, 'Bleu': 12, 'Jaune': 7 } # Change 8 to 7 because yellow is invisible and make my eyes bleeding...


class UnoGame(game.Game):
	"""
		Constructor
		
		Creates parent class configuration and add commands
		Creates timers and start "wait-for-play" one
	"""
	def __init__(self, name, client, priority=None, events={}):
		game.Game.__init__(self, name, client, priority=priority, events=events)
		self.gameid = 'uno'
		self.config = game.GameConfig(os.path.dirname(os.path.abspath(__file__)), 'uno')
		
		for key in self.config.cfgfile.options("Commands"):
			self.config.commands[self.config.basecfg['cmdprefix']+key] = getattr(self, self.config.cfgfile.get("Commands", key))
		self.setEnabled(False)
	
	def destroy(self):
		game.Game.destroy(self)
		if hasattr(self, 'penalty_timer'):
			if not self.penalty_timer is None:
				self.penalty_timer.stop()
			
	def reinit(self):
		self.facecard = []
		self.trash = []
		
		self.penalty_timer = None
		self.penalty = int(self.config.myconfig['penaltyiflong'])
		
		self.players = {}
		self.play_order = []
		self.active_player = -1
		self.running = False
		self.totalcards = []
		self.choice_color = False
		self.asked_color = ''
		
		self.timerecord = 0
		tr = self.data().get_min(self.gameid, "gameduration")
		if not tr is None:
			self.timerecord = tr.gameduration
		
		self.roundcount = 0
		self.finalorder = []
		game.Game.reinit(self)
			
	"""
		Registers a player into the game and stops "wait-for-play" timer when minimum players is reached
	"""
	def register_player(self, nickname, params):
		game.Game.register_player(self, nickname, params)
		if not self.running:
			self.players[nickname] = {
				'ingame': True,
				'cards': [],
				'points': 0,
				'uno': False,
				'record': None
			}
			
			self.info("* Getting record for " + nickname)
			rec = self.data().get_min(self.gameid, "gameduration", gamewinner=nickname)
			if not rec is None:
				self.info("** Record: " + util.seconds_to_string_time(rec.gamewintime))
				self.players[nickname]['record'] = rec.gamewintime
				
	"""
		Check number of players who are in game yet
	"""
	def check_players_ingame(self):
		result = 0
		for p in self.players:
			if self.players[p]['ingame']:
				result = result + 1
		return result
		
	"""
		Resets penalty timer object
	"""
	def restart_penalty_timer(self):
		if self.penalty_timer:
			self.penalty_timer.stop()
		self.penalty = int(self.config.myconfig['penaltyiflong'])
		self.penalty_timer = timer.BotTimer(1, self.penalty_timer_proc, ["PenaltyTimer"])
		self.penalty_timer.start()
			
	"""
		Main penalty timer procedure
	"""
	def penalty_timer_proc(self, args):
		curplayer = self.players[self.play_order[self.active_player]]
		self.penalty = self.penalty - 1
		if self.penalty == 0:
			self.give_multiple_cards(curplayer, 2)
			self.emit_pubmsg("\x034 * " + curplayer + " pioche\x02 2 cartes\x02 pour avoir attendu trop longtemps !\x03")
			self.penalty_timer.stop()
			self._next_turn()
			
	"""
		Redefines "pick_previous_player" method of game.Game
		Case of the 3 cards NOPLAY, +2, and +4 which makes next player not playing this turn if players are more than 2... where the previous player is not -1 but -2 !
	"""
	def pick_previous_player(self):
		if ((self.facecard[0] in ['noplay', '+2', '+4']) and (len(self.players) > 2)) or ((self.facecard[0] in ['chg', 'noplay']) and (len(self.players) == 2)):
			value = 2
		else:
			value = 1
		if self.sens == 0:
			idx = (self.active_player - value + (len(self.players) * 20)) % len(self.players)
		else:
			idx = (self.active_player + value + (len(self.players) * 20)) % len(self.players)
		self.info("Picking previous player: " + str(self.play_order[idx]))
		return self.play_order[idx]
		
	"""
		Starts effective game, distributes cards, and start the first player turn
	"""
	def start_game(self):
		game.Game.start_game(self)
		self.totalcards = []
		for cl in COLORS:
			for c in CARDS:
				if c != '0':
					for i in range(2):
						self.totalcards.append([c, cl])
				else:
					self.totalcards.append([c, cl])
		for s in SPECIAL:
			for i in range(4):
				self.totalcards.append([s, None])
				
		self.info("* " + str(len(self.players)) + " players registered.")
		self.info("* " + str(len(self.totalcards)) + " loaded cards.")
			
		random.shuffle(self.totalcards)
		random.shuffle(self.play_order)
		
		order = '* ORDRE DE JEU : '
		for o in self.play_order:
			order = order + o
			if o != self.play_order[-1]:
				order = order + ' -> '
		self.emit_pubmsg('\x033 ' + order + '\x03')
		
		self.distribute_cards()
		self.facecard = self.get_a_card()
		
		for p in self.players:
			self._show_cards_to_player(p)
			
		self._next_turn(show = False)
		
	"""
		Stops game and penalty timer
	"""
	def stop_game(self):
		if not self.penalty_timer is None:
			self.penalty_timer.stop()
		game.Game.stop_game(self)
	
	"""
		Notices cards to the player
	"""
	def _show_cards_to_player(self, player):
		res = "* Vos cartes  "
		for c in self.players[player]['cards']:
			res = res + " - " + self._card_to_str(c)
		self.emit_notice(player, res)
			
	"""
		Shows the facecard
	"""
	def _show_facecard(self):
		return self._card_to_str(self.facecard, face = True)
		
	"""
		Transform a card tuple into a string with mIRC colors
	"""
	def _card_to_str(self, card, face = False):
		result = card[0]
		if not card[0] in SPECIAL:
			result = "\x03" + str(COLORS[card[1]]) + " " + result + " (" + card[1] + ") \x03"
		elif (face) and (self.asked_color != ''):
			result = "\x03" + str(COLORS[self.asked_color]) + " " + result + " (" + self.asked_color + ") \x03"
		return result
	
	"""
		Next turn method
		Calls next_player method from parent class and shows facecard
	"""
	def _next_turn(self, show = True):
		if self.active_player < len(self.play_order) and self.active_player >= 0:
			cur = self.play_order[self.active_player]
			if len(self.players[cur]['cards']) == 1 and not self.players[cur]['uno']:
				self.emit_notice(cur, "\x034 * N'oubliez pas de dire UNO ! Tapez !uno avant que le joueur suivant ne joue ! \x03")
			
		nick = self.next_player()
		self.emit_pubmsg('\x02 \x032' + "* C'est au tour de " + nick + " de jouer !" + '\x03\x02')
		self.roundcount = self.roundcount + 1
		
		self.emit_pubmsg('\x02 ' + "* CARTE ACTUELLE: " + self._show_facecard() + '\x02')
		if show:
			self._show_cards_to_player(nick)
		# self.restart_penalty_timer()
			
	"""
		Show cards for a player
	"""
	def show_mycards(self, player, params):
		self.emit_notice(player, '\x02 ' + "* CARTE ACTUELLE: " + self._show_facecard() + '\x02')
		self._show_cards_to_player(player)
			
	"""
		Picks a card
	"""
	def get_a_card(self):
		if len(self.totalcards) == 0:
			for t in self.trash:
				self.totalcards.append(t)
				self.trash.remove(t)
		c = self.totalcards[0]		
		self.totalcards.remove(c)
		return c
		
	"""
		Pick card action
	"""
	def pick_card(self, player, params):
		if not player in self.players:
			self.emit_notice(player, "* Vous n'êtes pas inscrit pour cette partie !! O_o")
		else:
			if player.lower() == self.play_order[self.active_player].lower():
				can_play = False
				for c in self.players[player]['cards']:
					if self._can_play_card(c):
						can_play = True
						break
				if not can_play:
					self.counter_uno()
					card = self.give_card_to_player(player, params)
					self.emit_notice(player, "* Vous venez de piocher: " + self._card_to_str(card))
					self._show_cards_to_player(player)
					if self._can_play_card(card) and self.config.myconfig['allowplayafterpick']:
						self.emit_notice(player, "* Vous pouvez jouer la carte que vous venez de piocher !")
					else:
						self._next_turn()
				else:
					self.emit_notice(player, "* Tu n'as pas besoin de piocher ! Ouvre les yeux O_O !")
			else:
				self.emit_notice(player, "* Ce n'est pas encore ton tour de jouer ! Sois patient petit scarabée ^^")
			
	"""
		Puts a card in the trash
	"""
	def trash_card(self, card):
		self.trash.append(card)
		if card[0] in SPECIAL:
			card[1] = None
		
	"""
		Gives a card to a player (when picks)
	"""	
	def give_card_to_player(self, player, params):
		self.players[player]['uno'] = False
		newcard = self.get_a_card()
		self.players[player]['cards'].append(newcard)
		return newcard
			
	def give_multiple_cards(self, player, qty):
		for i in range(qty):
			self.give_card_to_player(player, params = None)
			
	"""
		Distributes all cards at the game start
	"""
	def distribute_cards(self):
		for p in self.players:
			for i in range(0, 7):
				self.give_card_to_player(p, params = None)

	"""
		Determines if the selected card can be played in the current context
	"""
	def _can_play_card(self, card):
		if (self.asked_color != '') and (not card[0] in SPECIAL) and (card[1].lower() == self.asked_color.lower()):
			can_play = True
		elif self.facecard[0].lower() == card[0].lower():
			can_play = True
		elif card[0] in SPECIAL:
			can_play = True
		elif (not card[1] is None) and (not self.facecard[1] is None) and (self.facecard[1].lower() == card[1].lower()):
			can_play = True
		elif (self.facecard[0] in SPECIAL) and (self.asked_color == ''):
			can_play = True
		else:
			can_play = False
		return can_play
		
	"""
		Checks if the current player with the current card is the winner, else goto next turn
	"""
	def check_winner(self, player, card):
		if len(self.players[player]['cards']) == 0 and self.players[player]['uno']:
			if self.winner == '':
				self.winner = player
				if self.total_time < self.timerecord:
					self.emit_pubmsg('\x02\x0312 * ' + player + " vient d'exploser le Record Absolu ! Félicitations !!" + '\x03\x02')
				elif self.players[player]['record'] is None or self.total_time < self.players[player]['record']:
					self.emit_pubmsg('\x02\x033 * ' + player + " bat son record de vitesse de victoire \\o/" + '\x03\x02')
				del self.players[player]
				self.play_order.remove(player)
				self.emit_pubmsg('\x02\x0310 * ' + player + " gagne la partie en " + util.seconds_to_string_time(self.total_time) + " ! *" + '\x03\x02')
				self.gamestatitem.gamewinner = player
				self.gamestatitem.gamewintime = self.total_time
			else:
				del self.players[player]
				self.play_order.remove(player)
				self.emit_pubmsg('\x02\x032 * ' + player + " a terminé en " + util.seconds_to_string_time(self.total_time) + " ! *" + '\x03\x02')
				
			self.pick_previous_player()
				
			if len(self.players) == 1:
				self.emit_pubmsg('\x02 ** PARTIE TERMINEE ** TEMPS TOTAL: ' + util.seconds_to_string_time(self.total_time) + ' (' + str(self.roundcount) + " tours de jeu)" + ' **\x02')
				if self.total_time < self.timerecord:
					self.emit_pubmsg("\x02\x0312 * Cette partie est la plus rapide de l'histoire du jeu ! Waouh O_O ! \x03\x02")
				self.emit_pubmsg('\x02\x033 ' + self.winner + ' remporte cette partie ! Félicitations :) ! ' + '\x03\x02')
				self.gamestatitem.gameduration = self.total_time
				self.gamestatitem.channel = self.param('channel')
				self.gamestatitem.gamedate = datetime.now().strftime("%d/%m/%Y")
				self.stop_game()
			else:
				self.interpret_next_turn(player, card)
		else:
			self.interpret_next_turn(player, card)
				
	"""
		Next turn procedure
	"""
	def interpret_next_turn(self, player, card):
		if card[0] in SPECIAL:
			self.emit_notice(player, "* Choisissez une couleur : !color R, B, J ou V")
			self.choice_color = True
		else:
			self.pick_previous_player()
			if card[0] == 'chg':
				if len(self.players) > 2:
					self.sens = (self.sens + 1) % 2
			self._next_turn(show = (card[0] != 'noplay' and (card[0] != 'chg' or len(self.players) > 2)))
			# self._next_turn(show = True)
			if card[0] == 'noplay':
				self.emit_pubmsg('\x02 * ' + self.play_order[self.active_player] + " passe son tour !!" + '\x02')
				if len(self.players[self.play_order[self.active_player]]['cards']) == 1:
					self.players[self.play_order[self.active_player]]['uno'] = True
				self._next_turn()
			elif card[0] == '+2':
				self.give_multiple_cards(self.play_order[self.active_player], 2)
				self.emit_pubmsg('\x02 * ' + self.play_order[self.active_player] + " pioche deux cartes !" + '\x02')
				if len(self.players) > 2:
					self._next_turn()
				else:
					self._show_cards_to_player(self.play_order[self.active_player])
			elif card[0] == 'chg' and len(self.players) == 2:
				self.emit_pubmsg('\x02 * ' + self.play_order[self.active_player] + " passe son tour !!" + '\x02')
				if len(self.players[self.play_order[self.active_player]]['cards']) == 1:
					self.players[self.play_order[self.active_player]]['uno'] = True
				self._next_turn()
		
	"""
		Counter Uno !
	"""
	def counter_uno(self):
		prev = self.pick_previous_player()							
		if len(self.players[prev]['cards']) == 1 and not self.players[prev]['uno']:
			self.emit_pubmsg('\x02 \x034' + "* " + prev + " pioche 2 cartes pour avoir oublié de dire UNO !!!" + '\x03 \x02')
			self.give_multiple_cards(prev, 2)
			
	"""
		Counter Uno action if not in automatic mode
	"""
	def say_counter_uno(self, player, params):
		if not self.config.myconfig['autocounteruno']:
			self.counter_uno()
		else:
			self.emit_notice(player, "\x034 * Le Contre-Uno est en mode AUTOMATIQUE.\x03")
		
	"""
		When a player quits the game, his cards are trashed
	"""
	def player_defects(self, player, params):
		for c in self.players[player]['cards']:
			self.trash_card(c)
		
	"""
		Plays a card
		
		Checks if the selected card can be played
	"""
	def play_card(self, player, params):
		if not player in self.players:
			self.emit_notice(player, "* Vous n'êtes pas inscrit pour cette partie !! O_o")
		else:
			if player.lower() == self.play_order[self.active_player].lower():
				try:
					card = self._parse_card_name(player, ' '.join(params))
					if card in self.players[player]['cards']:
						if (self.running) and (self.facecard) and (not card[0] is None) and ((not card[1] is None) or (card[0] in SPECIAL)):					
							if self._can_play_card(card):
								self.emit_pubmsg(' * ' + player + " joue la carte " + self._card_to_str(card))
								self.counter_uno()							
								self.asked_color = ''
								self.trash_card(self.facecard)
								self.players[player]['cards'].remove(card)
								if card in self.players[player]['cards'] and self.config.myconfig['allowdoubleoneplay']:
									self.players[player]['cards'].remove(card)
								self.facecard = card
								
								self.check_winner(player, card)
							else:
								self.emit_notice(player, "Vous ne pouvez pas jouer cette carte !")
					else:
						self.emit_notice(player, "Hey le coquin :3 T'essaies de jouer une carte que tu n'as pas -_-'...")
				except Exception:
					pass
			else:
				self.emit_notice(player, "* Ce n'est pas encore ton tour de jouer ! Sois patient petit scarabée ^^")

	"""
		Says the final "UNO!" when player got only one card in his hand
		Only if the player who said "uno" is the previous one...
	"""
	def say_uno(self, player, params):
		if self.running and player in self.players:
			if player.lower() == self.pick_previous_player().lower():
				if len(self.players[player]['cards']) == 1:
					self.emit_pubmsg(player + " : UNO !!!")
					self.players[player]['uno'] = True
				else:
					if self.config.myconfig['doublepickonbaduno']:
						self.emit_pubmsg('\x034 * ' + player + " dit UNO !!!! Mais... mais... il te reste plus d'une carte !! :o Petit malin ! Tiens prends deux cartes ! :3" + '\x03')
						self.give_multiple_cards(player, 2)
					else:
						self.emit_pubmsg('\x034 * ' + player + " dit UNO !!!! Mais... euh ... alors soit c'est trop tard, soit t'essaies de bluffer ptit coquin ! ^_^" + '\x03')
			else:
				self.emit_pubmsg('\x034 ' + "* Je sais bien que tu es pressé de jouer, " + player + ", mais là quand même..." + '\x03')
		
	"""
		Chooses a color in Color Choice Mode
	"""
	def choose_color(self, player, params):
		if self.choice_color:
			can_continue = True
			color = params[0].lower()
			if color == 'v':
				color = 'Vert'
			elif color == 'b':
				color = 'Bleu'
			elif color == 'j':
				color = 'Jaune'
			elif color == 'r':
				color = 'Rouge'
			else:
				self.emit_notice(player, "* Couleur inconnue.")
				can_continue = False
			
			if can_continue:
				self.emit_pubmsg('\x03' + str(COLORS[color]) + ' * ' + player + ' demande du ' + color + '! \x03')
				self.asked_color = color
				self.choice_color = False
				self._next_turn()
				if self.facecard[0] == '+4':
					self.give_multiple_cards(self.play_order[self.active_player], 4)
					self.emit_pubmsg('\x02 * ' + self.play_order[self.active_player] + " pioche quatre cartes !" + '\x02')
					if len(self.players) > 2:
						self._next_turn()
					else:
						self._show_cards_to_player(self.play_order[self.active_player])
	
	"""
		Cardname parsing
		
		A cardname typed by a player could have multiple forms
		For example, player want to play a Green 7
		:	7V / 7G / 7Vert / 7Green / 7 vert / 7 green must be ok !
		if player want to play a special card:
		:	+4 or joker
		if player want to play a special colored card:
		:	chgV / noplayG
	"""
	def _parse_card_name(self, source, cardname):
		value = None
		color = None
		error = False
		card = cardname.lower()
		# print source,":",cardname
		
		if card[0] == '+':
			if card[1] == '2':
				value = '+2'
				idx = 2
				if card[2] == ' ':
					idx = 3
				if card[idx] == 'v' or card[idx] == 'g' or card[idx:] == 'green' or card[idx:] == 'vert':
					color = 'Vert'
				elif card[idx] == 'b' or card[idx:] == 'bleu' or card[idx:] == 'blue':
					color = 'Bleu'
				elif card[idx] == 'r' or card[idx:] == 'rouge' or card[idx:] == 'red':
					color = 'Rouge'
				elif card[idx] == 'j' or card[idx] == 'y' or card[idx:] == 'jaune' or card[idx:] == 'yellow':
					color = 'Jaune'
			elif card[1] == '4':
				value = '+4'
				color = None
		elif card == 'joker':
			value = 'joker'
			color = None
		elif card[0:3] == 'chg':
			value = 'chg'
			idx = 3
			if card[3] == ' ':
				idx = 4
			if card[idx] == 'v' or card[idx] == 'g' or card[idx:] == 'green' or card[idx:] == 'vert':
				color = 'Vert'
			elif card[idx] == 'b' or card[idx:] == 'bleu' or card[idx:] == 'blue':
				color = 'Bleu'
			elif card[idx] == 'r' or card[idx:] == 'rouge' or card[idx:] == 'red':
				color = 'Rouge'
			elif card[idx] == 'j' or card[idx] == 'y' or card[idx:] == 'jaune' or card[idx:] == 'yellow':
				color = 'Jaune'
		elif card[0:6] == 'noplay':
			value = 'noplay'
			idx = 6
			if card[idx] == ' ':
				idx = 7
			if card[idx] == 'v' or card[idx] == 'g' or card[idx:] == 'green' or card[idx:] == 'vert':
				color = 'Vert'
			elif card[idx] == 'b' or card[idx:] == 'bleu' or card[idx:] == 'blue':
				color = 'Bleu'
			elif card[idx] == 'r' or card[idx:] == 'rouge' or card[idx:] == 'red':
				color = 'Rouge'
			elif card[idx] == 'j' or card[idx] == 'y' or card[idx:] == 'jaune' or card[idx:] == 'yellow':
				color = 'Jaune'
		else:
			try:
				value = str(int(card[0]))
				idx = 1
				if card[1] == ' ':
					idx = 2
				if card[idx] == 'v' or card[idx] == 'g' or card[idx:] == 'green' or card[idx:] == 'vert':
					color = 'Vert'
				elif card[idx] == 'b' or card[idx:] == 'bleu' or card[idx:] == 'blue':
					color = 'Bleu'
				elif card[idx] == 'r' or card[idx:] == 'rouge' or card[idx:] == 'red':
					color = 'Rouge'
				elif card[idx] == 'j' or card[idx] == 'y' or card[idx:] == 'jaune' or card[idx:] == 'yellow':
					color = 'Jaune'
			except ValueError:
				self.emit_notice(source, "* Carte " + cardname + " inconnue.")
				# raise
		# print value," ",color
		return [value, color]
		
	def get_help(self, player, params):
		self.emit_privmsg(player, "\x02* Bienvenue sur l'aide du module de jeu Uno pour ChokoBot *\x02")
		self.emit_privmsg(player, "* Liste des commandes disponibles:")
		self.emit_privmsg(player, "*   !play: S'inscrire pour la prochaine partie")
		self.emit_privmsg(player, "*   !help: Afficher ce message")
		self.emit_privmsg(player, "*   !card OU !c: Jouer une carte")
		self.emit_privmsg(player, "*   !pick: Piocher une carte")
		self.emit_privmsg(player, "*   !uno: Dire UNO !")
		self.emit_privmsg(player, "*   !color: Choisir une couleur lors d'un +4 ou d'un Joker")
		self.emit_privmsg(player, "*   !mycards: Affiche la carte actuelle du sabot, et les cartes de votre main")
		self.emit_privmsg(player, "*   !time: Permet de visualiser le temps écoulé depuis le début de la partie")
		self.emit_privmsg(player, "*   !rec: Permet de voir ses records ou celui d'un autre joueur")
		self.emit_privmsg(player, "*   !recs: Records généraux du jeu")
		self.emit_privmsg(player, "*   !pause / !resume: Mettre en pause / Enlever le mode pause")
		self.emit_privmsg(player, "*   !cuno: Contre-Uno ! (Uniquement dans le cas où il n'est pas automatique)")
		self.emit_privmsg(player, "*   !start: Forcer le démarrage de la partie avant la fin du compte à rebours")
		self.emit_privmsg(player, "* Les codes de cartes sont les suivants :")
		self.emit_privmsg(player, "*   Pour jouer une carte classique (ex: un 5 Rouge), il suffit de taper '!card 5R' ou '!card 5 rouge'")
		self.emit_privmsg(player, "*   La carte 'chg' correspond au Changement de Sens de jeu. Pour la jouer il suffit de taper '!card chgX' où X est la première lettre de la couleur")
		self.emit_privmsg(player, "*   La carte 'noplay' correspond à la carte Interdiction de Jouer. Pour la jouer il suffit de taper '!card noplayX' où X est la première lettre de la couleur")
		self.emit_privmsg(player, "*   Les cartes spéciales, '+4' et 'joker' se jouent sans couleur. Pour les jouer il suffit de taper '!card joker' ou '!card +4'.")
		self.emit_privmsg(player, "* Merci de fermer ce dialogue privé. Certains clients IRC diffusent les notices à l'intérieur des messages privés (XChat notamment).")
