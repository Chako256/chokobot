#-*- coding:utf-8 -*-
from mybot import module
import json
import re
import os.path

class BadwordModule(module.BotModule):
	def __init__(self, name, client, priority=None):
		module.BotModule.__init__(self, name, client, priority=priority)
		self.__conf = self.read_conf(filename="badword.json")

	def on_privmsg(self, serv, ev):
		command = ev.arguments()[0].strip().split()
		if command[0] == '.badword':
			self.debug("Request Badword command: " + command[1])
			if ev.source().ident in self.__conf.admins:
				self.debug(" Parsing command...")
				self.__parse_command(serv, ev.source().nickname, command[1], command[2:])
			else:
				self.debug(" [!] Forbidden access [!]")
				serv.notice(ev.source().nickname, "* Accès refusé *")

	def __parse_command(self, serv, source, command, params):
		if command in self.__conf.commands:
			cbk = getattr(self, self.__conf.commands.get(command))
			if cbk:
				cbk(serv, source, params)

	def add_badword(self, serv, source, params):
		bw = self.data().get_one("words", word=params[0].lower())
		self.debug("{}".format(bw))
		if bw is None:
			self.data().save(self.data().create_object("words", word=params[0].lower()))
			serv.notice(source, "* Badword \x02%s\x02 ajouté." % (params[0].lower(),))
		else:
			serv.notice(source, "\x034 [!] Ce badword existe déjà [!] \x03")

	def del_badword(self, serv, source, params):
		bw = self.data().get_one("words", word=params[0].lower())
		if bw is not None:
			self.data().delete("words", word=params[0].lower())
			serv.notice(source, "* Badword \x02%s\x02 supprimé." % (params[0].lower(),))
		else:
			serv.notice(source, "\x034 [!] Ce badword n'existe pas [!] \x03")

	def list_badword(self, serv, source, params):
		serv.notice(source, "* Liste des badwords *")
		for bw in self.data().get("words"):
			serv.notice(source, " - \x02%s\x02" % (bw.word,))

	def action(self, serv, ev):
		for bw in self.data().get("words"):
			if re.search(".*%s.*" % (bw.word.lower(),), ev.arguments()[0].lower()):
				serv.kick(ev.target(), ev.source().nickname, "Mot interdit détecté !")
				break

	def on_pubmsg(self, serv, ev):
		self.action(serv, ev)

	def on_pubaction(self, serv, ev):
		self.action(serv, ev)
