# -*- coding:utf-8 -*-

from mybot import module
from datetime import datetime


class SeenModule(module.BotModule):
	def __init__(self, name, client, priority=None):
		module.BotModule.__init__(self, name, client, priority=priority)

	def detect(self, server, sender, nickname, channel):
		if nickname == sender:
			server.privmsg(
				channel,
				"[Seen] La schizophrénie ça se soigne tu sais ?"
			)
		elif not server.server.is_user_on_channel(nickname, channel):
			userdata = self.data().get_one("seen",
				                           nickname=nickname,
				                           channel=channel)
			self.debug("Userdata fetched: {}".format(userdata))
			if userdata is not None:
				server.privmsg(
					channel,
					"[Seen] {} a été vu pour la dernière fois le {} à {}.".format(
						nickname,
						userdata.last_quit.strftime("%d/%m/%Y"),
						userdata.last_quit.strftime("%H:%M:%S")
					)
				)
			else:
				server.privmsg(
					channel,
					"[Seen] Aucune donnée pour {}".format(nickname)
				)
		else:
			server.privmsg(
				channel,
				"[Seen] Achète toi des lunettes ! {} est ici !".format(nickname)
			)

	def affect(self, server, event):
		nickname = event.source().nickname
		channel = event.target()
		userdata = self.data().get_one("seen",
			                           nickname=nickname,
			                           channel=channel)
		if userdata is None:
			self.debug("No data for {}. Creating...".format(nickname))
			userdata = self.data().create_object(
				"seen",
				nickname=nickname,
				channel=channel,
				last_quit=datetime.now()
			)
		else:
			self.debug("Found data for {}. Updating...".format(nickname))
			userdata.last_quit = datetime.now()
			userdata.channel=channel
		self.data().save(userdata)

	def on_quit(self, server, event):
		self.affect(server, event)

	def on_part(self, server, event):
		self.affect(server, event)

	def on_pubmsg(self, server, event):
		msg = event.arguments()[0]
		parts = msg.split()
		if parts[0] == "!seen":
			nickname = parts[1]
			self.detect(server, event.source().nickname, nickname, event.target())

