#-*- coding:utf-8 -*-

from mybot import module
from datetime import datetime
import json, os.path

###########################################################
## Bot Administrator                                     ##
## Special commands are delivered by private messages    ##
##  .say <channel> <message>                             ##
##  .act <channel> <message>                             ##
##  .module (enable|disable) <module> { <channel> }      ##
##  .control (stop|restart)                              ##
##  .uptime                                              ##
##  .rpg 												 ##
##  .join <channel>										 ##
###########################################################

class Administrator(module.BotModule):
	def __init__(self, name, client, priority=None):
		module.BotModule.__init__(self, name, client, priority=priority)
		self.__starttime = datetime.now()

		cfg = self.read_conf()
		self.__admins = cfg.admins
		self.__starter = cfg.starter

	def seconds_to_string_time(self, seconds):
		hr = int(seconds / 3600)
		mn = int(seconds / 60) - (hr * 60)
		sec = seconds - (mn * 60) - (hr * 3600)
		result = ""
		if hr > 0:
			result = result + str(hr) + " heure(s) "
		if mn > 0:
			result = result + str(mn) + " minute(s) "
		result = result + str(sec) + " seconde(s)"
		return result

	def __elapsed_uptime(self):
		now = datetime.now()
		delta = now - self.__starttime
		out = ""
		if delta.days > 0:
			out = out + str(delta.days) + " jour(s) "
		out = out + self.seconds_to_string_time(delta.seconds)
		return out

	def on_privmsg(self, server, event):
		pv = event.arguments()[0].strip()
		if pv != '' and pv.startswith(self.__starter):
			if event.source().ident in self.__admins:
				self.__parse_pv(server, event.source().nickname, pv)
			else:
				server.notice(event.source().nickname, "Accès refusé.")

	def __parse_pv(self, server, source, pv):
		pv_chunks = pv.split()
		if pv_chunks[0] == '.say':
			if len(pv_chunks) < 3:
				server.notice(source, "Usage: .say <channel> <message>")
			else:
				channel = pv_chunks[1]
				message = ' '.join(pv_chunks[2:])
				server.privmsg(channel, message)
		elif pv_chunks[0] == '.act':
			if len(pv_chunks) < 3:
				server.notice(source, "Usage: .act <channel> <message>")
			else:
				channel = pv_chunks[1]
				message = ' '.join(pv_chunks[2:])
				server.action(channel, message)
		elif pv_chunks[0] == '.module':
			if len(pv_chunks) < 3:
				server.notice(source, "Usage: .module (enable|disable|start|stop) <module> { <channel> }")
			else:
				controltag = pv_chunks[1]
				modulename = pv_chunks[2]
				channel = None if len(pv_chunks) == 3 else pv_chunks[3]

				if controltag == 'enable':
					result = server.enable_module(modulename, channel)
					if result:
						server.notice(source, "Module '%s' activé." % (modulename,))
					else:
						server.notice(source, "Impossible d'activer le module '%s'. Le verrou sur le canal '%s' a été refusé." % (modulename, channel))
				elif controltag == 'disable':
					server.disable_module(modulename)
					server.notice(source, "Module '%s' désactivé." % (modulename, ))
				elif controltag == 'stop':
					pass
				elif controltag == 'start':
					pass
		elif pv_chunks[0] == '.control':
			if len(pv_chunks) < 2:
				server.notice(source, "Usage: .control (stop|restart)")
			else:
				controltag = pv_chunks[1]
				if controltag == 'stop':
					server.request_stop()
				elif controltag == 'restart':
					server.request_restart()
		elif pv_chunks[0] == '.uptime':
			server.notice(source, "Allumé depuis le %(date)s - %(elapsed)s" % {
				'date': self.__starttime.strftime("%d/%m/%Y %H:%M:%S"),
				'elapsed': self.__elapsed_uptime()
			})
			server.notice(source, "  %d modules chargés" % (self.client().modulecount(), ))
		elif pv_chunks[0] == '.rpg':
			if len(pv_chunks) < 5:
				server.notice(source, "Usage: .rpg <submodule> <action> <property> <value>")
			else:
				submodule = pv_chunks[1]
				action = pv_chunks[2]
				property = pv_chunks[3]
				value = pv_chunks[4]
				if server.signal_module(self.name(), 'rpg', submodule, action=action, property=property, value=value):
					server.notice(source, "** Succès de l'opération **")
				else:
					server.notice(source, "** Echec de l'opération **")
		elif pv_chunks[0] == '.join':
			if len(pv_chunks) < 2:
				server.notice(source, "Usage: .join <channel>")
			else:
				channel = pv_chunks[1]
				server.join(channel)
		elif pv_chunks[0] == '.part':
			if len(pv_chunks) < 2:
				server.notice(source, "Usage: .part <channel>")
			else:
				channel = pv_chunks[1]
				server.part(channel)
		elif pv_chunks[0] == '.userlist':
			if len(pv_chunks) < 2:
				server.notice(source, "Usage: .userlist <channel>")
			else:
				channel = pv_chunks[1]
				server.notice(source, "Userlist of {}: {}".format(
					channel,
					server.get_user_list(channel, withmode=True)
				))

