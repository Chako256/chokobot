#-*- coding:utf-8 -*-

import os
import re
import json
import random
from datetime import datetime

from mybot import module

class Quotes(module.BotModule):
	def __init__(self, name, client, priority=None):
		module.BotModule.__init__(self, name, client, priority=priority)
		try:
			config = self.read_conf(filename="config.json")
			
			self.public_name = config.name
			self.commands = {}
			for cmd in config.commands:
				self.debug('Registering command %s' % (cmd))
				if hasattr(self, config.commands.get(cmd)):
					self.commands[cmd] = getattr(self, config.commands.get(cmd))
				else:
					self.debug('Command does not exist')

			self.help = config.help
			self.help_commands = config.commands
		except Exception as e:
			self.error('Unable to load config file : %s' % (str(e)))
			self.setEnabled(False)

	def last_id(self, server, chan):
		_last = self.data().get_max("quotes", "qid", server=server, channel=chan)
		if _last is None:
			return 0
		return _last.qid

	def addquote(self, nick, server, chan, content):
		"""
		Add a quote into the database

		Return a text message which can be send to the channel
		"""
		self.debug('%s from %s wants to add a quote' % (nick, chan))
		if len(content) > 0:
			try:
				qid = self.last_id(server, chan) + 1
				self.data().save(self.data().create_object("quotes", 
					qid=qid,
					nickname=nick,
					date=datetime.now(),
					text=content,
					server=server,
					channel=chan)
				)
				return "Quote %d ajoutée !" % (qid)
			except Exception as e:
				self.error(str(e))

			return "Quote non ajoutée. Essaie encore..."
		else:
			self.helpquote(nick, server, chan, 'addquote')

		return None

	def delquote(self, nick, server, chan, content):
		"""
		Delete a quote for a given ID
		"""
		self.debug('%s on %s wants to delete a quote' % (nick, chan))

		if len(content) > 0 and re.search('^[0-9]+$', content) is not None:
			try:
				dquote = self.data().get_one("quotes", server=server, channel=chan, qid=int(content))
				if dquote is not None:
					self.data().delete("quotes", _id=dquote.id())
					return "Quote %d supprimée :'(" % (int(content),)
				else:
					return "Quote %d non supprimée. Non trouvé." % (int(content),)
			except Exception as e:
				self.error(str(e))
		else:	
			self.helpquote(nick, server, chan, 'delquote')

		return None

	def getquote(self, nick, server, chan, content):
		"""
		Get a quote :
			- randomly if content if empty
			- by a given ID
			- by a search pattern

		Return a string which contains the quote or None if errors occur
		"""
		row = None
		if len(content) > 0:
			# Get the quote for a given ID
			if not re.search('^[0-9]+$', content) == None:
				row = self.data().get_one("quotes", server=server, channel=chan, qid=int(content))
			# Get a random quote according to a search pattern
			else:
				row = self.data().get_random("quotes", server=server, channel=chan, text={"$regex": content})
		# Get a random quote
		else:
			row = self.data().get_random("quotes", server=server, channel=chan)
		try:
			if row is not None:
				return "[%d] %s" % (row.qid, row.text)
		except Exception as e:
			self.error(str(e))

		return None

	def helpquote(self, nick, server, chan, content):
		"""
		Return the help message
		"""
		self.debug('The noob %s on %s wants some help' % (nick, chan))

		# Need the help for a given command (function name in the content)
		if len(content) > 0:
			cmds = []

			for k,v in self.help_commands.items():
				if v == content:
					cmds.append(k)
		else:
			cmds = self.help.keys()

		for cmd in cmds:
			txt = self.help[cmd].split('\n')
			for t in txt:
				self.client().notice(
					nick,
					"[ %s ] %s" % (self.public_name, t)
				)

		return None

	def infosquote(self, nick, server, chan, content):
		"""
		Display some usefull (or not) informations about a quote
		"""
		self.debug('%s on %s wants to know more about a quote' % (nick, chan))
		row = None
		if len(content) > 0 and re.search('^[0-9]+$', content) is not None:
			try:
				row = self.data().get_one("quotes", server=server, channel=chan, qid=int(content))
			except:
				row = None
			try:
				if row is not None:
					return "Quote %d ajoutée par %s le %s" % (
						row.qid,
						row.nickname,
						row.date.strftime('%d/%m/%Y à %H:%M')
					)
			except Exception as e:
				self.error(str(e))
		else:
			self.helpquote(nick, server, chan, 'infosquote')

		return None
	
	def on_pubmsg(self, server, event):
		result = None
		args = event.arguments()[0].split()
		if len(args) > 0:
			if args[0].startswith('!'):
				cmd = args[0][1:]
				if cmd in self.commands:
					func = self.commands.get(cmd)
					if func is not None:
						result = func(
							nick=event.source().ident,
							server=str(server.server),
							chan=event.target(),
							content=' '.join(args[1:])
						)

					if not result is None:
						server.privmsg(
							event.target(),
							result
						)
