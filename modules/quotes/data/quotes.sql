CREATE TABLE quotes (
q_id INT,
q_server TEXT,
q_chan TEXT,
q_nick TEXT,
q_date TIMESTAMP,
q_txt TEXT,
PRIMARY KEY (q_id, q_server, q_chan));
