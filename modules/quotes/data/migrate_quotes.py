#-*- coding:utf-8 -*-

import sqlite3
import json

db = sqlite3.connect('quotes.db')
out = { }

sql = """SELECT * FROM quotes ORDER BY q_server, q_chan, q_date"""
cur = db.cursor()
cur.execute(sql)
res = cur.fetchall()
last_server = ""
last_chan = ""

for row in res:
	buff = {
		'id': int(row[0]),
		'nick': row[3],
		'date': row[4],
		'txt': row[5]
	}
	if last_server != row[1]:
		last_server = row[1]
		if not last_server in out:
			out[last_server] = {}
	if last_chan != row[2]:
		last_chan = row[2]
		if not last_chan in out[last_server]:
			out[last_server][last_chan] = []
	out[last_server][last_chan].append(buff)

json.dump(out, open('quotes.data.json', 'w', encoding="utf-8"))

cur.close()
db.close()
