#-*- coding:utf-8 -*-

import unittest
from analyzer import PokerAnalyzer


class PokerHandTest(unittest.TestCase):
    def setUp(self):
        self.cards = [ 'As', 'Roi', 'Dame', 'Valet', '10', '9', '8', '7', '6', '5', '4', '3', '2' ]
        self.analyzer = PokerAnalyzer()

    def test_royal_flush(self):
        hand = [['As', 'Coeur'], ['Roi', 'Coeur'], ['Dame', 'Coeur'], ['Valet', 'Coeur'], ['10', 'Coeur'], ['4', 'Trèfle'], ['2', 'Pique']]
        analysis = self.analyzer.analyze(hand, self.cards)
        self.assertEqual(analysis[1], 1000)

    def test_straight_flush(self):
        hand = [['As', 'Pique'], ['Roi', 'Coeur'], ['Dame', 'Coeur'], ['Valet', 'Coeur'], ['10', 'Coeur'], ['9', 'Coeur'], ['2', 'Pique']]
        hand2 = [['As', 'Coeur'], ['Valet', 'Coeur'], ['10', 'Coeur'], ['9', 'Coeur'], ['8', 'Coeur'], ['7', 'Coeur'], ['4', 'Carreau']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 900)
        self.assertGreaterEqual(analysis2[1], 900)
        self.assertLess(analysis[1], 1000)
        self.assertLess(analysis2[1], analysis[1])

    def test_four_kind(self):
        hand = [['As', 'Pique'], ['As', 'Carreau'], ['As', 'Trèfle'], ['As', 'Coeur'], ['10', 'Pique'], ['7', 'Carreau'], ['2', 'Trèfle']]
        hand2 = [['Roi', 'Pique'], ['Roi', 'Trèfle'], ['Roi', 'Carreau'], ['Roi', 'Coeur'], ['2', 'Trèfle'], ['3', 'Carreau'], ['4', 'Pique']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 800)
        self.assertGreaterEqual(analysis2[1], 800)
        self.assertLess(analysis[1], 900)
        self.assertLess(analysis2[1], analysis[1])

    def test_full_house(self):
        hand = [['Roi', 'Pique'], ['Roi', 'Carreau'], ['Roi', 'Trèfle'], ['2', 'Carreau'], ['4', 'Pique'], ['4', 'Trèfle'], ['5', 'Coeur']]
        hand2 = [['Roi', 'Pique'], ['Roi', 'Carreau'], ['Roi', 'Trèfle'], ['2', 'Carreau'], ['2', 'Pique'], ['4', 'Trèfle'], ['5', 'Coeur']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 700)
        self.assertGreaterEqual(analysis2[1], 700)
        self.assertLess(analysis[1], 800)
        self.assertLess(analysis2[1], analysis[1])

    def test_straight(self):
        hand = [['Roi', 'Pique'], ['Dame', 'Trèfle'], ['Valet', 'Trèfle'], ['10', 'Carreau'], ['9', 'Pique'], ['4', 'Trèfle'], ['5', 'Coeur']]
        hand2 = [['Roi', 'Pique'], ['6', 'Carreau'], ['4', 'Trèfle'], ['2', 'Carreau'], ['3', 'Pique'], ['4', 'Trèfle'], ['5', 'Coeur']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 500)
        self.assertGreaterEqual(analysis2[1], 500)
        self.assertLess(analysis[1], 600)
        self.assertLess(analysis2[1], analysis[1])

    def test_flush(self):
        hand = [['Roi', 'Pique'], ['9', 'Pique'], ['Valet', 'Pique'], ['2', 'Pique'], ['9', 'Carreau'], ['4', 'Pique'], ['5', 'Coeur']]
        analysis = self.analyzer.analyze(hand, self.cards)
        self.assertGreaterEqual(analysis[1], 600)
        self.assertLess(analysis[1], 700)

    def test_three_of_a_kind(self):
        hand = [['Roi', 'Pique'], ['Roi', 'Trèfle'], ['Valet', 'Trèfle'], ['10', 'Carreau'], ['9', 'Pique'], ['4', 'Trèfle'], ['Roi', 'Coeur']]
        hand2 = [['Roi', 'Pique'], ['6', 'Carreau'], ['4', 'Trèfle'], ['5', 'Carreau'], ['3', 'Pique'], ['5', 'Trèfle'], ['5', 'Coeur']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 400)
        self.assertGreaterEqual(analysis2[1], 400)
        self.assertLess(analysis[1], 500)
        self.assertLess(analysis2[1], analysis[1])

    def test_double_pair(self):
        hand = [['Roi', 'Pique'], ['Roi', 'Trèfle'], ['5', 'Trèfle'], ['Valet', 'Carreau'], ['9', 'Pique'], ['Valet', 'Trèfle'], ['9', 'Coeur']]
        hand2 = [['Roi', 'Pique'], ['Roi', 'Trèfle'], ['Valet', 'Trèfle'], ['5', 'Carreau'], ['3', 'Pique'], ['5', 'Trèfle'], ['2', 'Coeur']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 300)
        self.assertGreaterEqual(analysis2[1], 300)
        self.assertLess(analysis[1], 400)
        self.assertLess(analysis2[1], analysis[1])

    def test_pair(self):
        hand = [['As', 'Pique'], ['As', 'Trèfle'], ['5', 'Trèfle'], ['10', 'Carreau'], ['7', 'Pique'], ['Valet', 'Trèfle'], ['9', 'Coeur']]
        hand2 = [['Roi', 'Pique'], ['Roi', 'Trèfle'], ['Valet', 'Trèfle'], ['6', 'Carreau'], ['3', 'Pique'], ['5', 'Trèfle'], ['2', 'Coeur']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 200)
        self.assertGreaterEqual(analysis2[1], 200)
        self.assertLess(analysis[1], 300)
        self.assertLess(analysis2[1], analysis[1])

    def test_upper_card(self):
        hand = [['As', 'Pique'], ['Valet', 'Trèfle'], ['5', 'Trèfle'], ['10', 'Carreau'], ['7', 'Pique'], ['2', 'Trèfle'], ['9', 'Coeur']]
        hand2 = [['Roi', 'Coeur'], ['4', 'Trèfle'], ['Valet', 'Carreau'], ['6', 'Carreau'], ['3', 'Pique'], ['5', 'Trèfle'], ['9', 'Coeur']]
        analysis = self.analyzer.analyze(hand, self.cards)
        analysis2 = self.analyzer.analyze(hand2, self.cards)
        self.assertGreaterEqual(analysis[1], 100)
        self.assertGreaterEqual(analysis2[1], 100)
        self.assertLess(analysis[1], 200)
        self.assertLess(analysis2[1], analysis[1])


if __name__ == '__main__':
    unittest.main()
