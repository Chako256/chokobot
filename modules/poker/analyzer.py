# -*- coding:utf-8 -*-

from collections import Counter
from itertools import groupby
from operator import itemgetter


class PokerAnalyzer(object):
    def groupby_arg(self, ix):
        return ix[0] + ix[1]
        
    def is_straight(self, hand):
        consecutive = []
        for k, g in groupby(enumerate(hand), self.groupby_arg):
            consecutive.append(list(g))
        for c in consecutive:
            if len(c) >= 5:
                return c
        return None

    def first_common(self, L1, L2):
        for i1 in L1:
            for i2 in L2:
                if i1 == i2:
                    return i1
        return None
        
    def analyze(self, hand, cards):
        hand_value = 0
        hand_name = None
        rcards = cards[::-1] # Revert the cards
        
        hand_values = [c[0] for c in hand]
        hand_colors = [c[1] for c in hand]
        
        sorted_hand_values = sorted(hand_values, key=lambda x: rcards.index(x), reverse=True)
        hand_value = 100 + sum([rcards.index(v) for v in sorted_hand_values])
        
        colors_counter = Counter(hand_colors)
        cnt_colors = colors_counter.most_common(1)[0]
        
        is_a_straight = self.is_straight(sorted(set([rcards.index(v) for v in sorted_hand_values]), reverse=True))
        
        if cnt_colors[1] >= 5:
            # There are 5 cards with the same color.
            color_cards = [rcards.index(c[0]) for c in hand if c[1] == cnt_colors[0]]
            if is_a_straight is not None:
                # This is a FLUSH !!
                if is_a_straight[0][1] == 12 and color_cards[0] == 12:
                    # This is a ROYAL FLUSH !!
                    hand_value = 1000
                    hand_name = "une \x02Quinte Flush Royale\x02"
                else:
                    high_card = self.first_common([s[1] for s in is_a_straight], color_cards)
                    hand_value = 900 + high_card
                    hand_name = "une \x02Quinte Flush au {}\x02".format(rcards[high_card])
            else:
                # Just a Color
                hand_value = 600 + (color_cards[0])
                hand_name = "une \x02Couleur\x02"
        else:
            if is_a_straight is not None:
                hand_value = 500 + is_a_straight[0][1]
                hand_name = "une \x02Suite au {}\x02".format(rcards[is_a_straight[0][1]])
            else:
                value_counter = Counter(sorted_hand_values)
                cnt_values = value_counter.most_common(2)
                if cnt_values[0][1] == 4:
                    # This is a Four of a Kind !!
                    hand_value = 800 + rcards.index(cnt_values[0][0]) 
                    hand_name = "un \x02Carré de {}\x02".format(cnt_values[0][0])
                elif cnt_values[0][1] == 3:
                    if cnt_values[1][1] == 2:
                        # This is a Full House !!
                        hand_value = 700 + (3 * rcards.index(cnt_values[0][0])) + (2 * rcards.index(cnt_values[1][0]))
                        hand_name = "un \x02Full au {} par les {}\x02".format(cnt_values[0][0], cnt_values[1][0])
                    else:
                        # This is a Three of a Kind !!
                        hand_value = 400 + rcards.index(cnt_values[0][0])
                        hand_name = "un \x02Brelan de {}\02".format(cnt_values[0][0])
                elif cnt_values[0][1] == 2:
                    if cnt_values[1][1] == 2:
                        # This is a Double Pair !!
                        hand_value = 300 + (3 * rcards.index(cnt_values[0][0])) + (2 * rcards.index(cnt_values[1][0]))
                        hand_name = "une \x02Double Paire de {} et de {}\x02".format(cnt_values[0][0], cnt_values[1][0])
                    else:
                        hand_value = 200 + rcards.index(cnt_values[0][0])
                        hand_name = "une \02Paire de {}\02".format(cnt_values[0][0])
        return (hand_name, int(hand_value))