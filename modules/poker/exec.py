# -*- coding:utf-8 -*-

from collections import Counter
from modules.common.game import game, util
from datetime import datetime
import random
import os
from .analyzer import PokerAnalyzer

COLORS = { 'Coeur': 4, 'Carreau': 4, 'Trèfle': 1, 'Pique': 1 }
CARDS = [ 'As', 'Roi', 'Dame', 'Valet', '10', '9', '8', '7', '6', '5', '4', '3', '2' ]
# CARDS = [ '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Valet', 'Dame', 'Roi', 'As' ]

class PokerGame(game.Game):
    """
    Poker Texas Hold'em

    @since 5.4
    """
    def __init__(self, name, client, priority=None, events={}):
        game.Game.__init__(self, name, client, priority=priority, events=events)
        self.gameid = 'poker'
        self.config = game.GameConfig(os.path.dirname(os.path.abspath(__file__)), 'poker')
        
        for key in self.config.cfgfile.options("Commands"):
            self.config.commands[self.config.basecfg['cmdprefix']+key] = getattr(self, self.config.cfgfile.get("Commands", key))
        self.setEnabled(False)

        self.analyzer = PokerAnalyzer()

    def reinit(self):
        self.running = False
        self.cards = []
        self.flop = []
        self.turn = None
        self.river = None

        self.sb = -1
        self.bb = -1

        self.small_blind = 1
        self.big_blind = self.small_blind * 2
        self.pot = 0

        self.timerecord = 0
        tr = self.data().get_min(self.gameid, "gameduration")
        if not tr is None:
            self.timerecord = tr.gameduration
            self.debug("Fastest game: {} seconds".format(self.timerecord))

        self.play_order = []
        self.active_player = -1

        self.max_bet = 0
        self.max_call = 0
        self.previous_call = 0

        self.flop_shown = False
        self.turn_shown = False
        self.river_shown = False

        game.Game.reinit(self)

        self.debug("Reinit: {}".format(self.players))

    def _card_to_str(self, card):
        result = "\x03" + str(COLORS[card[1]]) + " " + card[0] + " (" + card[1] + ")\x03"
        return result

    def register_player(self, nickname, params):
        if len(params) == 0 or int(params[0]) < 100 or int(params[0]) > 2000:
            self.emit_notice(nickname, "\x034 * Vous devez spécifier votre mise de départ entre 100 et 2000: !play 400 pour miser 400 Botcoins :)\x03")
            return None
        game.Game.register_player(self, nickname, params)
        if not self.running:
            self.players[nickname] = {
                'ingame': True,
                'cards': [],
                'money': int(params[0]),
                'status': 'in',
                'bet': 0,
                'allin': False,
                'talked': False,
                'handvalue': 0,
                'nickname': nickname,
                'record': None
            }
            self.info("* Getting record for " + nickname)
            rec = self.data().get_min(self.gameid, "gameduration", gamewinner=nickname)
            if rec is not None:
                self.info("** Record: " + util.seconds_to_string_time(rec.gamewintime))
                self.players[nickname]['record'] = rec.gamewintime

    def show_mycards(self, nickname, params=None):
        res = "* Vos cartes: "
        for c in self.players[nickname]['cards']:
            res = res + " - " + self._card_to_str(c)
        self.emit_notice(nickname, res)
        self.emit_notice(nickname, "\x0312 -> Argent: " + str(self.players[nickname]['money']) + " Bc\x03 | \x035Mise: " + str(self.players[nickname]['bet']) + " Bc\x03")
        self.analyze_hand(nickname)

    def rebuild_cards(self):
        self.cards = []
        for cl in COLORS:
            for c in CARDS:
                self.cards.append([c, cl])
        random.shuffle(self.cards)
        self.flop = []
        self.turn = None
        self.river = None
        self.max_bet = 0
        self.max_call = 0

        self.flop_shown = False
        self.turn_shown = False
        self.river_shown = False

    def start_game(self):
        game.Game.start_game(self)
        self.rebuild_cards()

        random.shuffle(self.play_order)
        order = " -> ".join(self.play_order)
        self.emit_pubmsg('\x033 ' + order + '\x03')

        self.new_subturn()

    def get_card(self):
        self.cards.pop(0) # Burnt card
        return self.cards.pop(0)

    def give_card_to_player(self, player):
        self.players[player]['cards'].append(self.get_card())

    def distribute_cards(self):
        for p in self.players:
            self.players[p]['cards'] = []
            for i in range(2):
                self.give_card_to_player(p)
        self.players[p]['cards'] = sorted(self.players[p]['cards'], key=lambda z: CARDS.index(z[0]))

    def not_naked_players(self):
        return [p for p in self.players if self.players[p]['status'] != 'naked']

    def check_winner(self):
        _in = self.not_naked_players()
        if len(_in) == 1:
            self.winner = _in[0]
            if self.total_time < self.timerecord:
                self.emit_pubmsg('\x02\x0312 * ' + self.winner + " vient d'exploser le Record Absolu ! Félicitations !!" + '\x03\x02')
            elif self.players[self.winner]['record'] is None or self.total_time < self.players[self.winner]['record']:
                self.emit_pubmsg('\x02\x033 * ' + self.winner + " bat son record de vitesse de victoire \\o/" + '\x03\x02')
            self.emit_pubmsg('\x02\x0310 * ' + self.winner + " gagne la partie en " + util.seconds_to_string_time(self.total_time) + " ! *" + '\x03\x02')
            self.gamestatitem.gamewinner = self.winner
            self.gamestatitem.gamewintime = self.total_time
            self.emit_pubmsg('\x02 ** PARTIE TERMINEE ** TEMPS TOTAL: ' + util.seconds_to_string_time(self.total_time) + ' **\x02')
            if self.total_time < self.timerecord:
                self.emit_pubmsg("\x02\x0312 * Cette partie est la plus rapide de l'histoire du jeu ! Waouh O_O ! \x03\x02")
            self.emit_pubmsg('\x02\x033 ' + self.winner + ' remporte cette partie ! Félicitations :) ! ' + '\x03\x02')
            self.gamestatitem.gameduration = self.total_time
            self.gamestatitem.channel = self.param('channel')
            self.gamestatitem.gamedate = datetime.now().strftime("%d/%m/%Y")
            return True
        else:
            return False

    def new_subturn(self):
        self.rebuild_cards()
        self.distribute_cards()

        self.pot = 0
        for p in self.players:
            if self.players[p]['money'] > 0:
                self.players[p]['allin'] = False
                self.players[p]['status'] = 'in'
                self.players[p]['bet'] = 0
                self.players[p]['talked'] = False
                self.players[p]['handvalue'] = 0
            elif self.players[p]['status'] == 'in':
                self.players[p]['ingame'] = False
                self.players[p]['status'] = 'naked'
                self.emit_pubmsg("\x02\x034 * {} n'a plus d'argent et doit quitter la table !\x03\x02".format(p))
                self.play_order.remove(p)

        if self.check_winner():
            self.stop_game()
            return True

        if self.sb == -1:
            self.sb = 0
        self.sb = (self.sb + 1) % len(self.play_order)
        self.bb = (self.sb + 1) % len(self.play_order)

        self.debug("Small Blind: " + str(self.sb))
        self.debug("Big Blind: " + str(self.bb))

        if not self.put_in_pot(self.small_blind, self.play_order[self.sb]):
            self.allin(self.play_order[self.sb], onstart=True)
        self.emit_pubmsg("\x036 * {} est la petite blind et mise {} Bc\x03".format(self.play_order[self.sb], self.small_blind))
        self.big_blind = self.small_blind * 2
        if self.small_blind < 128:
            self.small_blind = self.big_blind
        if not self.put_in_pot(self.big_blind, self.play_order[self.bb]):
            self.allin(self.play_order[self.bb], onstart=True)
        self.emit_pubmsg("\x032 * {} est la big blind et mise {} Bc\x03".format(self.play_order[self.bb], self.big_blind))

        self.active_player = (self.bb + 1) % len(self.play_order)
        self.next_turn(increment=False)

    def put_in_pot(self, amount, source, is_a_call=False):
        if self.players[source]['money'] < amount:
            self.emit_pubmsg("\x02\x034 * Hey ! La maison ne fait pas crédit !\x03\x02")
            return False
        else:
            self.pot += amount
            self.players[source]['money'] -= amount
            self.players[source]['bet'] += amount
            self.debug(source + " puts " + str(amount) + " Bc on the table")

            if self.max_bet < self.players[source]['bet']:
                self.max_bet = self.players[source]['bet']
            if not is_a_call:
                self.previous_call = self.max_call
            self.max_call = amount

            if self.players[source]['money'] == 0:
                self.players[source]['allin'] = True
            return True

    def win_to_player(self, player, amount):
        self.players[player]['money'] += amount

    def turn_ended(self):
        _out = True
        for p in self.players:
            self.debug("Player {}: allin {} status '{}' bet {} (max: {}) talked {}".format(p, self.players[p]['allin'], self.players[p]['status'], self.players[p]['bet'], self.max_bet, self.players[p]['talked']))
            if (self.players[p]['status'] == 'in') and ((self.players[p]['allin'] == False and self.players[p]['bet'] < self.max_bet) or not self.players[p]['talked']):
                _out = False
            if not _out:
                break

        return _out

    def next_turn(self, increment=False, show=True):
        self.debug("Next turn. Players: " + str(self.players))
        can_play = False
        cur = None
        _continue = True

        _ended = self.turn_ended()
        if _ended and len(self.in_players()) > 1:
            _continue = self.check_subturn()
        elif len(self.in_players()) == 1 or (_ended and len([p for p in self.players if self.players[p]['allin'] == False]) == 1):
            _continue = False

        players_last = [p for p in self.players if self.players[p]['status'] == 'in']
        if _continue:
            if increment:
                self.active_player = (self.active_player + 1) % len(self.play_order)

            remaining = len(players_last)
            if remaining > 1:
                while not can_play:
                    self.debug("Current player: " + str(self.active_player))
                    cur = self.play_order[self.active_player]
                    if self.players[cur]['status'] == 'in' and self.players[cur]['ingame'] == True:
                        can_play = True
                    else:
                        self.active_player = (self.active_player + 1) % len(self.play_order)
                self.emit_pubmsg('\x02 \x032' + "* C'est au tour de " + cur + " de jouer !" + '\x03\x02')
                if self.flop is not None and len(self.flop) > 0:
                    self.show_flop()
                if self.turn is not None:
                    self.show_turn()
                if self.river is not None:
                    self.show_river()
                self.emit_pubmsg("\x02\x032 * Pot: {} Bc | Mise max: {} Bc".format(self.pot, self.max_bet))
                if show:
                    self.show_mycards(cur)
                self.flop_shown = False
                self.turn_shown = False
                self.river_shown = False
            else:
                self.emit_pubmsg("\x02\x0312 * {} gagne cette manche en remportant {} Bc !\x03\x02".format(players_last[0], self.pot))
                self.win_to_player(players_last[0], self.pot)
                self.new_subturn()
        else:
            self.emit_pubmsg("\x02\x033 * Les joueurs dévoilent leurs cartes !\x03\x02")
            for p in players_last:
                self.emit_pubmsg("\x0312 * {} -> {} et {}\x03".format(p, self._card_to_str(self.players[p]['cards'][0]), self._card_to_str(self.players[p]['cards'][1])))
                self.analyze_hand(p, public=True)

            ranking = sorted([self.players[p] for p in players_last], key=lambda z: z['handvalue'], reverse=True)
            ex_aequo = [ranking[i] for i in range(1, len(ranking)) if ranking[i]['handvalue'] == ranking[0]['handvalue']]
            ex_aequo.insert(0, ranking[0])

            if len(ex_aequo) == 1:
                self.emit_pubmsg("\x02\x0312 * {} gagne cette manche en remportant {} Bc !\x03\x02".format(ranking[0]['nickname'], self.pot))
                self.win_to_player(ranking[0]['nickname'], self.pot)
            else:
                self.pot = self.pot - (self.pot % len(ex_aequo))
                self.emit_pubmsg("\x02\x035 * {} sont ex-aequo et se partagent le pot, soit {} Bc chacun !\x03\x02".format(", ".join([e['nickname'] for e in ex_aequo]), int(self.pot/len(ex_aequo))))
                for e in ex_aequo:
                    self.win_to_player(e['nickname'], int(self.pot / len(ex_aequo)))
            self.new_subturn()

    def show_flop(self):
        if not self.flop_shown:
            self.emit_pubmsg("\x02 " + "* FLOP: " + " - ".join([self._card_to_str(c) for c in self.flop]))
            self.flop_shown = True

    def show_turn(self):
        if not self.turn_shown:
            self.emit_pubmsg("\x02 " + "* TURN: " + self._card_to_str(self.turn))
            self.turn_shown = True

    def show_river(self):
        if not self.river_shown:
            self.emit_pubmsg("\x02 " + "* RIVER: " + self._card_to_str(self.river))
            self.river_shown = True

    def in_players(self):
        return [p for p in self.players if self.players[p]['status'] == 'in']

    def analyze_hand(self, player, public=False):
        player_hand = self.players[player]['cards']
        if self.flop is not None and len(self.flop) > 0:
            player_hand = player_hand + self.flop
        if self.turn is not None:
            player_hand.append(self.turn)
        if self.river is not None:
            player_hand.append(self.river)

        analysis = self.analyzer.analyze(player_hand, CARDS)
        if analysis[0] is not None:
            if not public:
                self.emit_notice(player, "\x0312 -> Vous avez {} !\x03".format(analysis[0]))
            else:
                self.emit_pubmsg("\x0312 * {} a {} !\x03".format(player, analysis[0]))
        self.players[player]['handvalue'] = analysis[1]

    def check_subturn(self):
        _out = False
        _all_allin = len([p for p in self.in_players() if self.players[p]['allin'] == False]) <= 1
        if self.flop is None or len(self.flop) == 0:
            self.flop = sorted([self.get_card() for i in range(3)], key=lambda z: CARDS.index(z[0]))
            _out = not _all_allin
            self.emit_pubmsg("\x033 * Le croupier dévoile le \x02flop\x02...\x03")
            self.show_flop()

        if not _out and self.turn is None:
            self.turn = self.get_card()
            _out = not _all_allin
            self.emit_pubmsg("\x033 * Le croupier dévoile la \x02turn\x02...\x03")
            self.show_turn()
        
        if not _out and self.river is None:
            self.river = self.get_card()
            _out = not _all_allin
            self.emit_pubmsg("\x033 * Le croupier dévoile la \x02river\x02...\x03")
            self.show_river()

        self.reset_talk()
        return _out

    def reset_talk(self, unless=None):
        for p in self.players:
            if ((unless is None) or (unless is not None and p not in unless)) and self.players[p]['status'] == 'in':
                self.players[p]['talked'] = False

    def talk(self, nickname, reset_others=None):
        self.players[nickname]['talked'] = True
        if reset_others is not None:
            if reset_others == True:
                reset_others = [nickname]
            self.reset_talk(unless=reset_others)

    def not_your_turn(self, nickname):
        self.emit_notice(nickname, "\x034* Ce n'est pas encore ton tour de jouer ! Sois patient petit scarabée ^^\x03")

    def call(self, nickname, params):
        if self.running:
            if nickname.lower() == self.play_order[self.active_player].lower():
                if self.max_bet > self.players[nickname]['bet']:
                    self.emit_pubmsg("\x02\x0312 * " + nickname + " suit !\x03\x02")
                    if self.put_in_pot(self.max_bet-self.players[nickname]['bet'], nickname, is_a_call=True):
                        self.talk(nickname)
                        self.next_turn(increment=True)
                else:
                    self.emit_notice(nickname, "\x034 * Tu n'as pas besoin de suivre puisque tu as déjà la meilleure enchère. Fais !ck ou !check si tu veux rester dans la partie sans relancer.\x03")
            else:
                self.not_your_turn(nickname)

    def check(self, nickname, params):
        if self.running:
            if nickname.lower() == self.play_order[self.active_player].lower():
                self.debug("Bet for {}: {}".format(nickname, self.players[nickname]['bet']))
                self.debug("Max Bet: {}".format(self.max_bet))
                if self.players[nickname]['bet'] == self.max_bet or self.players[nickname]['allin'] == True:
                    self.emit_pubmsg("\x02\x032 * " + nickname + " checke !\x03\x02")
                    self.talk(nickname)
                    self.next_turn(increment=True)
                else:
                    self.emit_notice(nickname, "\x034 * Vous ne pouvez pas checker ! Vous avez misé {} Bc et la mise minimale est de \x02{} Bc\x02\x03".format(self.players[nickname]['bet'], self.max_bet))
            else:
                self.not_your_turn(nickname)

    def _raise(self, nickname, params):
        if self.running:
            if nickname.lower() == self.play_order[self.active_player].lower():
                self.debug("Max Call: {}".format(self.max_call))
                self.debug("Big Blind: {}".format(self.big_blind))
                self.debug("Previous Call: {}".format(self.previous_call))
                if self.max_call == self.big_blind:
                    min_call = self.max_call * 2
                else:
                    min_call = self.max_call + (self.max_call - self.previous_call)
                self.debug("Min Call: {}".format(min_call))
                if (len(params) == 0 or int(params[0]) < min_call):
                    self.emit_notice(nickname, "\x034 * Vous devez spécifier une valeur supérieure ou égale à {} Bc".format(min_call))
                else:
                    self.emit_pubmsg("\x02\x033 * {} relance de {} Bc !\x03\x02".format(nickname, params[0]))
                    if self.put_in_pot(int(params[0]), nickname):
                        self.debug("Max Call: {}".format(self.max_call))
                        self.debug("Big Blind: {}".format(self.big_blind))
                        self.debug("Previous Call: {}".format(self.previous_call))
                        self.talk(nickname, reset_others=True)
                        self.next_turn(increment=True)
            else:
                self.not_your_turn(nickname)

    def fold(self, nickname, params):
        if self.running:
            if nickname.lower() == self.play_order[self.active_player].lower():
                self.emit_pubmsg("\x02\x034 * {} se couche !\x03\x02".format(nickname))
                self.players[nickname]['status'] = 'out'
                self.next_turn(increment=True)
            else:
                self.not_your_turn(nickname)

    def allin(self, nickname, params=None, onstart=False):
        if self.running:
            if onstart or nickname.lower() == self.play_order[self.active_player].lower():
                if self.players[nickname]['allin'] == True:
                    self.emit_pubmsg("\x034 * Euh... t'es déjà au \x02tapis\x02...\x03")
                else:
                    self.emit_pubmsg("\x02\x0310 * {} fait tapis !\x03\x02".format(nickname))
                    if self.put_in_pot(self.players[nickname]['money'], nickname):
                        if not onstart:
                            if self.max_bet > self.players[nickname]['money']:
                                self.talk(nickname)
                            else:
                                self.talk(nickname, reset_others=[p for p in self.players if self.players[p]['allin'] == False])
                            self.next_turn(increment=True)
            else:
                self.not_your_turn(nickname)

    def get_help(self, nickname, params):
        self.emit_privmsg(nickname, "\x02* Bienvenue sur l'aide du module de jeu Poker pour ChokoBot *\x02")
        self.emit_privmsg(nickname, "* Les règles appliquées sont celles du \x033Texas Hold'em\x03 *")
        self.emit_privmsg(nickname, "* Liste des commandes disponibles:")
        self.emit_privmsg(nickname, "*   !play <money>: S'inscrire pour la prochaine partie avec un capital de départ de <money> Bc")
        self.emit_privmsg(nickname, "*   !help: Afficher ce message")
        self.emit_privmsg(nickname, "*   !c ou !call: Suivre la mise")
        self.emit_privmsg(nickname, "*   !f ou !fold: Se coucher")
        self.emit_privmsg(nickname, "*   !a ou !allin: Faire tapis")
        self.emit_privmsg(nickname, "*   !r ou !raise <money>: Relancer de <money> Bc")
        self.emit_privmsg(nickname, "*   !ck ou !check: Checker le tour (peut être fait uniquement si votre mise est égale à la mise max)")
        self.emit_privmsg(nickname, "*   !mycards: Affiche les cartes de votre main, votre argent restant et votre mise.")
        self.emit_privmsg(nickname, "*   !time: Permet de visualiser le temps écoulé depuis le début de la partie")
        self.emit_privmsg(nickname, "*   !rec: Permet de voir ses records ou celui d'un autre joueur")
        self.emit_privmsg(nickname, "*   !recs: Records généraux du jeu")
        self.emit_privmsg(nickname, "*   !pause / !resume: Mettre en pause / Enlever le mode pause")
        self.emit_privmsg(nickname, "*   !start: Forcer le démarrage de la partie avant la fin du compte à rebours")
        self.emit_privmsg(nickname, "* Merci de fermer ce dialogue privé. Certains clients IRC diffusent les notices à l'intérieur des messages privés (XChat notamment).")
