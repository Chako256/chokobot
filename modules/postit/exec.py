#-*- coding:utf-8 -*-

# Post-It module for notes

from mybot import module
import json, os.path
from datetime import datetime

class PostItModule(module.BotModule):
	def __init__(self, name, client, priority=None):
		module.BotModule.__init__(self, name, client, priority=priority)

	def usage(self, sender):
		"""
		Usage help notices
		"""
		self.client().notice(sender, "Créer une note: !postit <dd/mm/yyyy> <contenu>")
		self.client().notice(sender, "Lister les notes: !listit")
		self.client().notice(sender, "Supprimer une note: !unpostit <numéro>")
		return None

	def on_join(self, server, event):
		"""
		Callback when a user joins
		"""
		post_count = self.data().get_count("posts", nickname=event.source().nickname, date={"$gt": int(datetime.now().timestamp())})
		if post_count > 0:
			self.client().notice(
				event.source().nickname, 
				"[ Post-It ] Vous avez {} note(s) enregistrée(s). Tapez !listit pour les lire.".format(
					post_count
				)
			)

	def on_pubmsg(self, server, event):
		if len(event.arguments()[0].strip()) > 0:
			chunks = event.arguments()[0].split()
			if chunks[0] == '!postit':
				if len(chunks) < 3:
					self.usage(event.source().nickname)
				else:
					try:
						dt = datetime.strptime(chunks[1], '%d/%m/%Y')
						if dt < datetime.now():
							raise ValueError()
					except ValueError:
						self.client().notice(event.source().nickname, '\x034 [Erreur] Date invalide. \x03')
						return None
					index = self.data().get_count("posts", nickname=event.source().nickname) + 1
					self.data().save(self.data().create_object("posts",
						index=index,
						nickname=event.source().nickname,
						date=int(dt.timestamp()),
						content=' '.join(chunks[2:])
					))
					self.client().notice(event.source().nickname, "Note ajoutée: Numéro %d." % (index, ))
			elif chunks[0] == '!listit':
				data = self.data().get("posts", nickname=event.source().nickname, date={"$gt": int(datetime.now().timestamp())})
				dlen = self.data().get_count("posts", nickname=event.source().nickname, date={"$gt": int(datetime.now().timestamp())})
				if dlen > 0:
					for pi in data:
						self.client().notice(event.source().nickname, "[ %d ] Jusqu'au %s: %s" % (pi.index, datetime.fromtimestamp(int(pi.date)).strftime('%d/%m/%Y'), pi.content))
				else:
					self.client().notice(event.source().nickname, "** Vous n'avez aucune note enregistrée.")
			elif chunks[0] == '!unpostit':
				if len(chunks) < 2:
					self.usage(event.source().nickname)
				else:
					num = int(chunks[1])
					search = self.data().get("posts", nickname=event.source().nickname, index=num)
					if search is None:
						self.client().notice("[ Erreur ] Note numéro %d introuvable." % (num, ))
					else:
						self.data().delete("posts", nickname=event.source().nickname, index=num)
						self.client().notice(event.source().nickname, "Note numéro %d supprimée." % (num, ))
