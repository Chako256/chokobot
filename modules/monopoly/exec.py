#-*- coding:utf-8 -*-

# Monopoly Game

import random, json, os, os.path, locale
from modules.common.game import game
from modules.common.game import util
from mybot import timer
from datetime import datetime

try:
	locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")
except locale.Error: # Windows does not like "fr_FR" neither "UTF-8" ><"
	locale.setlocale(locale.LC_ALL, "")

class MonopolyWork:
	def __init__(self, workitem):
		proc = workitem.get('call')
		params = workitem.get('params')
		self.__proc = proc
		self.__params = params

	def proc(self):
		return self.__proc

	def params(self):
		return self.__params

class MonopolyProperty:
	def __init__(self, position, name, work):
		self.__name = name
		self.__owner = None
		self.__work = MonopolyWork(work)
		self.type = None
		self.__position = position
		self.__buildable = False

	def buildable(self):
		return self.__buildable

	def set_buildable(self, flag):
		self.__buildable = flag

	def position(self):
		return self.__position

	def work(self):
		return self.__work

	def owner(self):
		return self.__owner

	def setOwner(self, owner):
		self.__owner = owner

	def name(self):
		return self.__name

class MonopolySpecialProperty(MonopolyProperty):
	def __init__(self, position, name, work):
		MonopolyProperty.__init__(self, position, name, work)
		self.type = 'special'

class MonopolyMortgageableProperty(MonopolyProperty):
	def __init__(self, position, name, work, price):
		MonopolyProperty.__init__(self, position, name, work)
		self.__price = price
		self.__mortgage = int(price / 2)
		self.__mortgaged = False

	def mortgage(self):
		return self.__mortgage

	def is_mortgaged(self):
		return self.__mortgaged

	def set_mortgaged(self, flag):
		self.__mortgaged = flag

	def price(self):
		return self.__price

class MonopolyColorProperty(MonopolyMortgageableProperty):
	def __init__(self, position, name, work, color, price, rent, rent1, rent2, rent3, rent4, rent5, houseprice):
		MonopolyMortgageableProperty.__init__(self, position, name, work, price)
		self.type = 'property'
		self.__color = color
		self.__price = price
		self.__mortgage = int(price / 2)
		self.rent0 = rent
		self.rent1 = rent1
		self.rent2 = rent2
		self.rent3 = rent3
		self.rent4 = rent4
		self.rent5 = rent5

		self.__housecount = 0
		self.__houseprice = houseprice
		
		self.set_buildable(True)

	def houseprice(self):
		return self.__houseprice

	def houses(self):
		return self.__housecount

	def hotel(self):
		return self.__housecount == 5

	def color(self):
		return self.__color

	def rent(self):
		return getattr(self, 'rent%d' % (self.__housecount,))

	def build_house(self):
		if self.__housecount < 5:
			self.__housecount += 1
			return True
		else:
			return False

	def destroy_house(self):
		if self.__housecount > 0:
			self.__housecount -= 1
			return True
		else:
			return False

	def house_label(self):
		if self.__housecount == 0:
			return 'Terrain nu'
		elif self.__housecount < 5:
			return '%d maison(s)' % (self.__housecount,)
		else:
			return '1 Hôtel'

class MonopolyStationProperty(MonopolyMortgageableProperty):
	def __init__(self, position, name, work, price, rent1, rent2, rent3, rent4):
		MonopolyMortgageableProperty.__init__(self, position, name, work, price)
		self.type = 'station'
		self.__price = price
		self.rent1 = rent1
		self.rent2 = rent2
		self.rent3 = rent3
		self.rent4 = rent4

	def rent(self, value):
		return getattr(self, 'rent%d' % (value, ))

class MonopolyCompanyProperty(MonopolyMortgageableProperty):
	def __init__(self, position, name, work, price, rent1, rent2):
		MonopolyMortgageableProperty.__init__(self, position, name, work, price)
		self.type = 'company'
		self.__price = price
		self.rent1 = rent1
		self.rent2 = rent2

	def rent(self, value):
		return getattr(self, 'rent%d' % (value,))

class MonopolyCard:
	def __init__(self, label, work, handled = None):
		self.__label = label
		self.__work = MonopolyWork(work)
		self.__handled = handled

	def handled(self):
		return self.__handled

	def work(self):
		return self.__work

	def label(self):
		return self.__label

class MonopolyBoard:
	def __init__(self, board):
		self.__data = json.load(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "conf", "board", "%s.json" % (board, )), encoding="utf-8"))
		self.__properties = []
		self.__chance = []
		self.__community = []
		self.__general = {}

		idx = 0
		for p in self.__data['properties']:
			px = None
			if p['type'] == 'property':
				px = MonopolyColorProperty(idx, p['name'], p['work'], p['color'], p['price'], p['rent'], p['rent1'], p['rent2'], p['rent3'], p['rent4'], p['rent5'], p['houseprice'])
			elif p['type'] == 'station':
				px = MonopolyStationProperty(idx, p['name'], p['work'], p['price'], p['rent1'], p['rent2'], p['rent3'], p['rent4'])
			elif p['type'] == 'company':
				px = MonopolyCompanyProperty(idx, p['name'], p['work'], p['price'], p['rent1'], p['rent2'])
			elif p['type'] == 'special':
				px = MonopolySpecialProperty(idx, p['name'], p['work'])

			if px:
				self.debug("[Board]  -> Adding %s (%s)" % (type(px), px.name()))
				self.__properties.append(px)
				idx += 1

		self.debug("[Board] %d properties added." % (len(self.__properties),))

		for c in self.__data['chance']:
			self.__chance.append(MonopolyCard(c['label'], c['work'], c.get('handled')))

		for c in self.__data['community']:
			self.__community.append(MonopolyCard(c['label'], c['work'], c.get('handled')))

		for x in self.__data:
			if not x in [ 'properties', 'community', 'chance' ]:
				self.__general[x] = self.__data[x]

	def property(self, idx):
		if idx >= 0 or idx < len(self.__properties):
			return self.__properties[idx]
		else:
			return None

	def color_properties(self, color):
		return [ p for p in self.__properties if type(p) == MonopolyColorProperty and p.color() == color ]

	def stations(self):
		return [ p for p in self.__properties if type(p) == MonopolyStationProperty ]

	def companies(self):
		return [ p for p in self.__properties if type(p) == MonopolyCompanyProperty ]

	def properties(self, nickname):
		return [ p for p in self.__properties if p.owner() == nickname ]

	def data(self, key):
		return self.__general[key]

	def size(self):
		return len(self.__properties)

	def chance(self):
		return self.__chance

	def community(self):
		return self.__community

	def random_chance(self):
		x = random.choice(self.__chance)
		while x.handled():
			x = random.choice(self.__chance)
		return x

	def random_community(self):
		x = random.choice(self.__community)
		while x.handled():
			x = random.choice(self.__community)
		return x

class MonopolyAuction:
	def __init__(self, property):
		self.__property = property
		self.__max_auction = {
			'nickname': '',
			'amount': 0
		}

	def property(self):
		return self.__property

	def bid(self, player, amount):
		if player != self.__max_auction['nickname'] and amount > self.__max_auction['amount']:
			self.__max_auction['nickname'] = player
			self.__max_auction['amount'] = amount
			return True
		else:
			return False

	def winner(self):
		return self.__max_auction

class MonopolyExchange:
	def __init__(self, asker):
		self.__asker = asker
		self.__target = None
		self.__asker_list = {
			'properties': [],
			'money': 0
		}
		self.__target_list = {
			'properties': [],
			'money': 0
		}
		self.waiting_state = False

	def waiting(self):
		return self.waiting_state

	def set_waiting(self, flag):
		self.waiting_state = flag

	def add_item(self, sender, item):
		if item.startswith('p'):
			if sender == self.__asker:
				self.__asker_list['properties'].append(int(item[1:]))
			elif sender == self.__target:
				self.__target_list['properties'].append(int(item[1:]))
		elif item.startswith('m'):
			if sender == self.__asker:
				self.__asker_list['money'] += int(item[1:])
			elif sender == self.__target:
				self.__target_list['money'] += int(item[1:])

	def clear(self):
		self.__asker_list['properties'].clear()
		self.__asker_list['money'] = 0
		self.__target_list['properties'].clear()
		self.__target_list['money'] = 0

	def asker(self):
		return self.__asker

	def target(self):
		return self.__target

	def set_target(self, target):
		self.__target = target

	def items(self, sender):
		if sender == self.__asker:
			return self.__asker_list
		elif sender == self.__target:
			return self.__target_list
		else:
			return []

class MonopolyGame(game.Game):
	def __init__(self, name, client, priority=None, events={}):
		game.Game.__init__(self, name, client, priority=priority, events=events)
		self.gameid = 'monopoly'
		self.config = game.GameConfig(os.path.dirname(os.path.abspath(__file__)), "monopoly")

		for key in self.config.cfgfile.options("Commands"):
			self.config.commands[self.config.basecfg['cmdprefix']+key] = getattr(self, self.config.cfgfile.get("Commands", key))
		for key in self.config.cfgfile.options("PrivateCommands"):
			self.config.privatecommands[self.config.basecfg['cmdprefix']+key] = getattr(self, self.config.cfgfile.get("PrivateCommands", key))
		self.setEnabled(False)

	def reinit(self):
		self.__board = MonopolyBoard(self.config.myconfig['activeboard'])
		self.players = {}
		self.play_order = []
		self.active_player = -1
		self.running = False
		self.dices = [0, 0]

		self.doublecount = 0
		self.timerecord = 0
		self.freepark = 0
		self.auction = None
		self.auction_timer = None
		self.auction_counter = self.config.myconfig['auctioncounter']

		self.manage_mode = False
		self.bankrupter = None
		self.exchange_mode = False

		self.exchange = None

		tr = self.data().get_max(self.gameid, "gameduration")
		if not tr is None:
			self.timerecord = tr.gameduration

		self.roundcount = 0
		self.finalorder = []
		game.Game.reinit(self)

	def register_player(self, nickname, params):
		game.Game.register_player(self, nickname, params)
		if not self.running:
			self.players[nickname] = {
				'nickname': nickname,
				'ingame': True,
				'possessions': {},
				'record': None,
				'jail': False,
				'outjail_comm': False,
				'outjail_chance': False,
				'money': 0,
				'position': 0,
				'nomoney': False,
				'requestchoice': False,
				'jailtries': 0,
				'buyorauc': False
			}

	def start_game(self):
		game.Game.start_game(self)
		random.shuffle(self.play_order)

		order = '* ORDRE DE JEU : '
		for o in self.play_order:
			order = order + o
			if o != self.play_order[-1]:
				order = order + ' -> '
		self.emit_pubmsg('\x033 ' + order + '\x03')

		for n in self.players:
			self.players[n]['money'] = self.__board.data('startmoney')
		self.active_player = 0
		self.do_turn()

	def _next_turn(self):
		if len(self.dices) > 0:
			self.check_winner()
		if len(self.dices) == 0 or self.dices[0] != self.dices[1]:
			self.active_player = (self.active_player + 1) % len(self.players.keys())
		if not self.check_winner():
			self.do_turn()

	def check_winner(self):
		if len(self.players) == 1:
			self.emit_pubmsg('\x02 ** PARTIE TERMINEE ** TEMPS TOTAL: ' + util.seconds_to_string_time(self.total_time) + ' **\x02')
			if self.total_time < self.timerecord:
				self.emit_pubmsg("\x02\x0312 * Cette partie est la plus rapide de l'histoire du jeu ! Waouh O_O ! \x03\x02")
			self.emit_pubmsg('\x02\x033 ' + self.winner + ' remporte cette partie ! Félicitations :) ! ' + '\x03\x02')
			self.gamestatitem.gameduration = self.total_time
			self.gamestatitem.gamedate = datetime.now().strftime("%d/%m/%Y")
			self.gamestatitem.channel = self.param('channel')
			self.gamestatitem.gamewinner = self.winner
			self.gamestatitem.gamewintime = self.total_time
			try:
				self.save_statitem()
			except Exception:
				pass
			self.stop_game()
			return True
		else:
			return False

	def current_position(self, player):
		return self.__board.property(player['position'])

	def pay(self, player, amount, destination = None, nodest = False):
		sold = player['money'] - amount
		if sold > 0:
			if destination is None:
				if not nodest:
					self.freepark += amount
			else:
				self.get_player(destination)['money'] += amount
			player['money'] -= amount
			self.debug("**** %s pays %d %s !!" % (player['nickname'], amount, self.__board.data('currency')))
		else:
			self.bankrupter = destination
			player['nomoney'] = True
			self.emit_pubmsg("\x034* %s n'a pas assez d'argent pour payer ! *" % (player['nickname'], ))
			self.emit_notice(player['nickname'], " * Tapez !manage pour gérer vos possessions *")

	def can_pay(self, player, amount):
		return player['money'] >= amount

	def earn(self, player, amount):
		player['money'] += amount
		self.debug("**** %s earns %d %s !!" % (player['nickname'], amount, self.__board.data('currency')))

	def player_houses(self, player):
		props = self.__board.properties(player['nickname'])
		total = sum([ p.houses() for p in props if hasattr(p, 'houses') and p.houses() < 5 ])
		return total

	def player_hotels(self, player):
		props = self.__board.properties(player['nickname'])
		total = sum([ 1 for p in props if hasattr(p, 'houses') and p.houses() == 5 ])
		return total

	def send_to_jail(self, player):
		player['jail'] = True
		player['position'] = 10
		self.emit_pubmsg("\x034\x02* %s va droit en prison !\x02\x03" % (player['nickname'], ))
		self.doublecount = 0
		self.dices = [ -1, -2 ]

	def out_jail(self, player, params):
		if player.lower() == self.play_order[self.active_player].lower():
			p = self.get_player(player)
			if p['jail'] and (p['outjail_chance'] or p['outjail_comm']):
				p['jail'] = False
				self.emit_pubmsg("\x033* %s utilise sa carte \x02Sortie de prison\x02 et peut jouer dès maintenant !\x03")
				if p['outjail_chance']:
					p['outjail_chance'] = False
				if p['outjail_comm']:
					p['outjail_comm'] = False
				self.do_turn()

	def do_turn(self):
		p = self.players[self.play_order[self.active_player]]
		self.emit_pubmsg("\x0312\x02 ** C'est au tour de %s de jouer **" % (p['nickname'],))
		if p['jail']:
			self.emit_pubmsg("\x036 -- %s est en prison. --\x03" % (p['nickname'],))
			if p['outjail_chance'] or p['outjail_comm']:
				self.emit_notice(p['nickname'], "\x033 * Vous avez une carte \x02Sortie de prison\x02. Tapez \x02!out\x02 pour l'utiliser !\x03")
		self.emit_notice(p['nickname'], "\x036 -- Patrimoine: %s %s" % (locale.format("%d", p['money'], True), self.__board.data('currency')))
		self.emit_notice(p['nickname'], " Tapez !roll pour lancer les dés ou !manage pour gérer vos propriétés")

	def notify_position(self, player):
		prop = self.__board.property(player['position'])
		self.display_board_item(player, prop)

	def grant_startcase(self, player):
		player['money'] += self.__board.data('startpropmoney')
		self.emit_pubmsg("\x033* %s reçoit \x02%s %s\x02 en passant par la Case Départ !\x03" % (player['nickname'], locale.format("%d", self.__board.data('startpropmoney'), True), self.__board.data('currency')))

	def move_player(self, player, value):
		destination = player['position'] + value
		if destination < 0:
			destination += self.__board.size()
		elif destination > self.__board.size():
			self.grant_startcase(player)
		player['position'] = destination % self.__board.size()
		self.notify_position(player)

	def move_player_to(self, player, index):
		player['position'] = index
		self.notify_position(player)

	def roll_dice(self, nickname, params):
		if not nickname in self.players:
			self.emit_notice(nickname, "\x034* Vous n'êtes pas inscrit pour cette partie !")
		else:
			if nickname.lower() == self.play_order[self.active_player].lower():
				p = self.get_player(nickname)
				if not p['buyorauc']:
					double_triggered = False
					self.dices[0] = int(random.randint(100, 600) / 100)
					self.dices[1] = int(random.randint(100, 600) / 100)
					
					self.emit_pubmsg("\x036* Résultats des dés: \x02%d / %d\x02" % (self.dices[0], self.dices[1]))
					if self.dices[0] == self.dices[1]:
						self.doublecount += 1
						double_triggered = True
					else:
						self.doublecount = 0		

					if not p['jail']:
						if self.doublecount == int(self.config.myconfig['doublecounttojail']):
							double_triggered = False
							self.emit_pubmsg("\x034\x02* %s a fait %d doubles de suite !\x02\x03" % (nickname, self.config.myconfig['doublecounttojail']))
							self.send_to_jail(p)
							self._next_turn()
						else:
							self.move_player(p, sum(self.dices))
					else:
						if not double_triggered:
							p['jailtries'] += 1
							if p['jailtries'] == self.config.myconfig['triesinjail']:
								self.emit_pubmsg("\x034* %s n'a pas réussi à faire de double ! La facture est de %s %s !" % (p['nickname'], locale.format("%d", self.config.myconfig['jailpay'], True), self.__board.data('currency')))
								self.pay(player, self.config.myconfig['jailpay'])
							else:
								self.emit_pubmsg("\x034* %s n'a pas fait de double ! Désolé, tu restes en prison !" % (p['nickname'], ))
								self._next_turn()
						else:
							self.emit_pubmsg("\x033* %s fait un double et sort de prison !\x03" % (p['nickname'],))
							p['jail'] = False
							self.move_player(p, sum(self.dices))
			else:
				self.emit_notice(nickname, "\x034* La patience est une vertu...\x03")

	def work_on_start_case(self, player, property, params):
		amount = self.__board.data('startpropmoney')
		if self.__board.data('doubleonstart'):
			amount += self.__board.data('startpropmoney')
		self.earn(player, amount)
		self.emit_pubmsg('\x033* %s reçoit \x02%s %s\x02 en tombant sur la case Départ !\x03' % (player['nickname'], locale.format("%d", amount, True), self.__board.data('currency')))
		self._next_turn()

	def display_board_item(self, player, item):
		cl = 1
		if hasattr(item, 'color'):
			cl = item.color()
		ln = "\x031,%(color)d     \x03\x02  %(name)s  \x02\x031,%(color)d     \x03" % { 'color': cl, 'name': item.name() }
		if hasattr(item, 'price'):
			ln += " -- Prix: \x02%s %s\x02" % (locale.format("%d", item.price(), True), self.__board.data('currency'))
		self.emit_pubmsg(ln)
		suppln = ""
		if item.type != 'special':
			if item.buildable():
				suppln += "\x036 -- (%s) --\x03" % (item.house_label(),)
			if isinstance(item, MonopolyMortgageableProperty) and item.is_mortgaged():
				suppln += "  \x02(Hypothéquée)\x02"
			if suppln != '':
				self.emit_pubmsg(suppln)
		wk = getattr(self, item.work().proc())
		wk(player, item, item.work().params())

	def color_monopoly(self, player, color, naked = False):
		props = self.__board.color_properties(color)
		cnt = 0
		for px in props:
			if px.owner() == player['nickname'] and ((naked and px.houses() == 0) or (not naked)):
				cnt += 1
		return cnt == len(props)

	def work_on_buyable_property(self, player, property):
		if property.owner() is None:
			self.emit_pubmsg("\x0312 -- Vacant --\x03")
			self.emit_notice(player['nickname'], " * Tapez !buy pour acheter, !auc pour lancer les enchères. *")
			player['buyorauc'] = True
			return True
		elif property.owner() != player['nickname']:
			self.emit_pubmsg("\x0312 -- Propriétaire: \x02%s\x02 -- \x03" % (property.owner(),))
			return False
		elif property.owner() == player['nickname'] and not property.is_mortgaged():
			self.emit_notice(player['nickname'], "\x0312 -- Vous êtes chez vous ! --\x03")
			return True

	def work_on_property(self, player, property, params):
		free = self.work_on_buyable_property(player, property)
		if not free:
			xrent = property.rent()
			if self.color_monopoly(player, property.color(), naked = True):
				xrent *= 2
			self.emit_pubmsg("\x034 * %s doit payer un loyer de \x02%s %s\x02 à %s !\x03" % (player['nickname'], locale.format("%d", xrent, True), self.__board.data('currency'), property.owner()))
			self.pay(player, xrent, property.owner())
			if not player['nomoney']:
				self._next_turn()

	def work_on_station(self, player, property, params):
		free = self.work_on_buyable_property(player, property)
		if not free:
			owner = property.owner()
			ostations = [ s for s in self.__board.stations() if s.owner() == owner ]
			xrent = property.rent(len(ostations))
			self.emit_pubmsg("\x034 * %s doit payer un loyer de \x02%s %s\x02 à %s !\x03" % (player['nickname'], locale.format("%d", xrent, True), self.__board.data('currency'), property.owner()))
			self.pay(player, xrent, property.owner())
			if not player['nomoney']:
				self._next_turn()

	def work_on_company(self, player, property, params):
		free = self.work_on_buyable_property(player, property)
		if not free:
			owner = property.owner()
			ocomps = [ c for c in self.__board.companies() if c.owner() == owner ]
			xrent = property.rent(len(ocomps)) * sum(self.dices)
			self.emit_pubmsg("\x034 * %s doit payer un loyer de \x02%s %s\x02 à %s !\x03" % (player['nickname'], locale.format("%d", xrent, True), self.__board.data('currency'), property.owner()))
			self.pay(player, xrent, property.owner())
			if not player['nomoney']:
				self._next_turn()

	def work_on_community(self, player, property, params):
		card = self.__board.random_community()
		self.emit_pubmsg("\x033 * %s pioche une carte Caisse de Communauté : \02%s\x02\x03" % (player['nickname'], card.label()))

		wk = card.work()
		getattr(self, wk.proc())(player, card, wk.params())

	def work_on_tax(self, player, property, params):
		tx = self.__board.data('tax')
		self.emit_pubmsg("\x034 * %s doit payer les \x02impôts sur le revenu\x02 soit %s %s !\x03" % (player['nickname'], locale.format("%d", self.__board.data('tax'), True), self.__board.data('currency')))
		self.pay(player, self.__board.data('tax'))
		if not player['nomoney']:
			self._next_turn()

	def work_on_freepark(self, player, property, params):
		self.earn(player, self.freepark)
		self._next_turn()

	def work_on_jail(self, player, property, params):
		self._next_turn()
		
	def work_on_gotojail(self, player, property, params):
		self.send_to_jail(player)
		self._next_turn()

	def work_on_chance(self, player, property, params):
		card = self.__board.random_chance()
		self.emit_pubmsg("\x037 * %s pioche une carte Chance : \x02%s\x02\x03" % (player['nickname'], card.label()))

		wk = card.work()
		getattr(self, wk.proc())(player, card, wk.params())

	def work_on_luxury(self, player, property, params):
		self.emit_pubmsg("\x034 * %s doit payer \x02%s %s\x02 au titre de la taxe de luxe !" % (player['nickname'], locale.format("%d", self.__board.data('luxury'), True), self.__board.data('currency')))
		self.pay(player, self.__board.data('luxury'))
		if not player['nomoney']:
			self._next_turn()

	def work_card(self, player, card, params):
		nextturn = True
		type = params['type']
		if type == 'pay':
			if params.get('value'):
				self.pay(player, params['value'])
			elif params.get('house') and params.get('hotel'):
				total = params['house'] * self.player_houses(player)
				total += params['hotel'] * self.player_hotels(player)
				self.pay(player, total)
		elif type == 'earn':
			self.earn(player, params['value'])
		elif type == 'move':
			if params.get('direction') and params['direction'] == 'backward':
				self.move_player_to(player, params['position'])
			elif params.get('direction') and params['direction'] == 'forward':
				self.move_player(player, params['position'] - player['position'])
			nextturn = False
		elif type == 'outjail':
			card.handled = True
			player['outjail'] += 1
		elif type == 'birthday':
			for p in self.players:
				if p != player['nickname']:
					self.pay(self.get_player(p), params['value'], player['nickname'])
		elif type == 'choice':
			player['requestchoice'] = params['pay']
			self.emit_notice(player['nickname'], "\x0312 * Tapez !pay pour payer, ou !chance pour tirer une carte Chance.\x03")
			nextturn = False

		if nextturn and not player['nomoney']:
			self._next_turn()

	def get_help(self, player, params):
		pass

	def pay_tax(self, player, params):
		p = self.get_player(player)
		if p['requestchoice'] != False:
			self.pay(p, p['requestchoice'])
			p['requestchoice'] = False

	def get_chance(self, player, params):
		p = self.get_player(player)
		if p['requestchoice'] != False:
			self.work_on_chance(p, None, params)

	def add_property_to_player(self, player, property):
		if type(property) == MonopolyColorProperty:
			cl = str(property.color())
		elif type(property) in [ MonopolyStationProperty, MonopolyCompanyProperty ]:
			cl = "1"
		else:
			cl = "0"

		if not cl in player['possessions']:
			player['possessions'][cl] = []
		player['possessions'][cl].append(property)
		property.setOwner(player['nickname'])

	def remove_property_from_player(self, player, property):
		cl = str(property.color())
		player['possessions'][cl].remove(property)
		property.setOwner(None)

	def give_property_to_player(self, player, property, destination):
		cl = str(property.color())
		player['possessions'][cl].remove(property)

		if not cl in destination['possessions']:
			destination['possessions'][cl] = []
		destination['possessions'][cl].append(property)
		property.setOwner(destination['nickname'])

	def buy_property(self, player, params):
		if self.running:
			if player.lower() == self.play_order[self.active_player].lower():
				p = self.get_player(player)
				if p['buyorauc']:
					prop = self.current_position(p)
					if self.can_pay(p, prop.price()):
						self.pay(p, prop.price(), nodest=True)
						self.add_property_to_player(p, prop)
						self.emit_pubmsg("\x033 * %s achète \x02%s\x02 pour %s %s !\x03" % (player, prop.name(), locale.format("%d", prop.price(), True), self.__board.data('currency')))
						p['buyorauc'] = False
						self._next_turn()
					else:
						self.emit_notice(player, "\x034 * Impossible. Fonds insuffisants.\x03")

	def start_auction(self, player, params):
		if self.running:
			if player.lower() == self.play_order[self.active_player].lower():
				p = self.get_player(player)
				if p['buyorauc']:
					prop = self.current_position(p)
					self.auction = MonopolyAuction(prop)

					self.emit_pubmsg("\x0312*-*- Vente aux enchères -*-*- %s -*-*\x03" % (prop.name(),))
					self.emit_pubmsg(" -- Tapez \x02!bid <somme>\x02 pour enchérir --")

					self.auction_counter = int(self.config.myconfig['auctioncounter'])
					p['buyorauc'] = False

	def bid_auction(self, player, params):
		if self.running:
			if hasattr(self, 'auction') and not self.auction is None and self.auction_counter > 0:
				try:
					p = self.get_player(player)
					if len(params) == 0:
						bid = 100
					else:
						bid = int(params[0])
					if self.can_pay(p, bid) and self.auction.bid(player, bid):
						self.auction_counter = int(self.config.myconfig['auctioncounter'])
						self.emit_pubmsg("\x0312 -- %s fait la plus grosse enchère avec \x02%s %s\x02 ! --\x03" % (locale.format("%d", bid, True), self.__board.data("currency")))
						if not hasattr(self, 'auction_timer') or self.auction_timer is None:
							self.auction_timer = timer.BotTimer(1, self.auction_proc, kwargs = { 'property': self.auction.property() })
							self.auction_timer.start()
					else:
						self.emit_notice(player, "\x034 -- Enchère invalide. Vous ne pouvez pas surenchérir sur vous-même ou votre enchère n'est pas assez élevée. --\x03")
				except ValueError:
					self.emit_notice(player, "\x034** Petit malin, je vais me fâcher tout rouge si t'écris des conneries ! è_é\x03")
			else:
				self.emit_notice(player, "\x034** Tu aimes les enchères, je sais... mais là c'est pas le moment :)\x03")

	def auction_proc(self, **kwargs):
		if not self.auction is None:
			self.auction_counter -= 1
			if self.auction_counter == 0:
				self.auction_timer.stop()
				del self.auction_timer

				winner = self.get_player(self.auction.winner()['nickname'])
				self.add_property_to_player(winner, kwargs['property'])
				winner['money'] -= self.auction.winner()['amount']

				self.emit_pubmsg("\x033\x02*-*- !! ADJUGE !! -*-*\x02\x03")
				self.emit_pubmsg("\x0312* \x02%s\x02 remporte l'enchère et obtient \x02%s\x02 pour \x02%s %s\x02 !\x03" % (winner['nickname'], kwargs['property'].name(), locale.format("%d", self.auction.winner()['amount'], True), self.__board.data('currency')))
				del self.auction

				self._next_turn()
			elif self.auction_counter == 10:
				self.emit_pubmsg("\x034\x02 * 1 FOIS !\x02\x03")
			elif self.auction_counter == 5:
				self.emit_pubmsg("\x0312\x02 ** 2 FOIS ! **\x02\x03")

	def list_properties(self, player, params = None, private = False):
		py = self.get_player(player)
		if private:
			proc = self.emit_privmsg
		else:
			proc = self.emit_notice
		proc(player, "\x036 -- Vos propriétés --\x03")
		for color in py['possessions']:
			ln = "   \x031,%s   \x03" % (color, )
			for prop in py['possessions'][color]:
				ln += " %s [%d] -" % (prop.name(), prop.position())
			proc(player, ln)

	def enter_manage(self, player, params):
		if self.running:
			if player.lower() == self.play_order[self.active_player].lower():
				self.manage_mode = True
				self._waiting_for_private = True
				self.emit_pubmsg("\x036 -- %s entre dans la gestion de ses propriétés --\x03" % (player,))
				self.list_properties(player, private = True)
				self.emit_privmsg(player, "\x033 \x02!buyh\x02 pour acheter des maisons, \x02!sell\x02 pour en vendre, \x02!mg\x02 pour gérer les hypothèques, \x02!bk\x02 pour déclarer la banqueroute, \x02!exit\x02 pour retourner au jeu.\x03")

	def buy_house(self, player, params):
		if self.running and self.manage_mode:
			if len(params) == 0 or params[0] == "help":
				self.emit_privmsg(player, "\x033 * Acheter une maison sur une propriété : !buyh house <numéro>\x03")
			elif len(params) > 0:
				tag = params[0]
				me = self.get_player(player)
				if len(params) > 1:
					if tag == "house":
						idx = int(params[1])
						prop = self.__board.property(idx)
						if prop:
							if self.color_monopoly(me, prop.color()):
								if self.can_pay(me, prop.houseprice()):
									if prop.owner().lower() == player.lower():
										if not prop.is_mortgaged():
											if prop.build_house():
												self.pay(me, prop.houseprice(), nodest=True)
												self.emit_privmsg(player, "\x033 * Maison construite sur \x02%s\x02 *\x03" % (prop.name().upper(),))
												self.emit_pubmsg("\x036 -- %s vient de constuire une maison sur \x02%s\x02\x03" % (player, prop.name(),))
											else:
												self.emit_privmsg(player, "\x034 * Impossible de construire une maison ici ! Il y a déjà un hôtel ! *\x03")
										else:
											self.emit_privmsg(player, "\x034 * Impossible de construire sur une propriété hypothéquée !\x03")
									else:
										self.emit_privmsg(player, "\x034 * Petit malin... è_é\x03")
								else:
									self.emit_privmsg(player, "\x034 * Impossible de construire une maison ! Vous n'avez pas les fonds suffisants !\x03")
							else:
								self.emit_privmsg(player, "\x034 * Impossible de construire une maison ici. Vous devez avoir le monopole de la couleur !\x03")
						else:
							self.emit_privmsg(player, "\x034 * Numéro %d invalide *\x03" % (idx, ))
					else:
						self.emit_privmsg(player, "\x034 * '%s' : Commande inconnue." % (tag,))

	def sell_house(self, player, params):
		if self.running and self.manage_mode:
			if len(params) == 0 or params[0] == "help":
				self.emit_privmsg(player, "\x033 * Vendre une maison sur une propriété : !sell house <numéro>\x03")
				self.emit_privmsg(player, "\x033 * Vendre la propriété : !sell prop <numéro>\x03")
			elif len(params) > 0:
				tag = params[0]
				me = self.get_player(player)
				if len(params) > 1:
					idx = int(params[1])
					prop = self.__board.property(idx)
					if prop:
						if tag == "house":
							if prop.owner().lower() == player.lower():
								if prop.destroy_house():
									self.earn(me, prop.houseprice())
									self.emit_privmsg(player, "\x033 * Maison vendue sur \x02%s\x02 *\x03" % (prop.name().upper(), ))
									self.emit_pubmsg("\x036 -- %s vient de vendre ue maison sur \x02%s\x02\x03" % (player, prop.name()))
								else:
									self.emit_privmsg(player, "\x034 * Impossible de vendre une maison ici ! Il n'y en a pas !\x03")
							else:
								self.emit_privmsg(player, "\x034 * Petit malin... è_é\x03")
						elif tag == "prop":
							if prop.owner().lower() == player.lower():
								if prop.houses() == 0:
									self.remove_property_from_player(me, prop)
									self.emit_privmsg(player, "\x033 * Propriété vendue ! *\x03")
									self.emit_pubmsg("\x036 -- %s vient de vendre \x02%s\x02\x03" % (player, prop.name()))
								else:
									self.emit_privmsg(player, "\x034 * Impossible de vendre une propriété où des maisons sont présentes ! \x03")
							else:
								self.emit_privmsg(player, "\x034 * Petit malin... è_é\x03")
						else:
							self.emit_privmsg(player, "\x034 * '%s' : Commande inconnue." % (tag,))
					else:
						self.emit_privmsg(player, "\x034 * Numéro %d invalide *\x03" % (idx, ))

	def mortgage_manage(self, player, params):
		if self.running and self.manage_mode:
			if len(params) == 0 or params[0] == 'help':
				self.emit_privmsg(player, "\x033 * Hypothéquer une propriété : !mg yes <numéro>\x03")
				self.emit_privmsg(player, "\x033 * Lever une hypothèque : !mg no <numéro>\x03")
			elif len(params) > 0:
				tag = params[0]
				me = self.get_player(player)
				if len(params) > 1:
					idx = int(params[1])
					prop = self.__board.property(idx)
					if prop:
						if prop.owner().lower() == player.lower():
							if tag == 'yes':
								if not prop.is_mortgaged():
									prop.set_mortgaged(False)
									self.earn(me, prop.mortgage())
									self.emit_privmsg(player, "\x033 * Hypothèque créée sur \x02%s\x02 *\x03" % (prop.name().upper(),))
									self.emit_pubmsg("\x036 -- %s vient d'hypothéquer sur \x02%s\x02\x03" % (player, prop.name(), ))
								else:
									self.emit_privmsg(player, "\x034 * La propriété est déjà hypothéquée !\x03")
							elif tag == 'no':
								if prop.is_mortgaged():
									if self.can_pay(me, prop.mortgage()):
										prop.set_mortgaged(False)
										self.pay(me, prop.mortgage())
										self.emit_privmsg(player, "\x033 * Hypothèque levée sur \x02%s\x02 *\x03" % (prop.name().upper(),))
										self.emit_pubmsg("\x036 -- %s vient de lever l'hypothèque sur \x02%s\x02\x03" % (player, prop.name(), ))
									else:
										self.emit_privmsg(player, "\x034 * Fonds insuffisants pour lever l'hypothèque !\x03")
								else:
									self.emit_privmsg(player, "\x034 * La propriété n'est pas hypothéquée !\x03")
							else:
								self.emit_privmsg(player, "\x034 * '%s' : Commande inconnue." % (tag,))
						else:
							self.emit_privmsg(player, "\x034 * Petit malin... è_é\x03")
					else:
						self.emit_privmsg(player, "\x034 * Numéro %d invalide *\x03" % (idx, ))


	def bankrupt_me(self, player, params):
		if self._waiting_for_private and self.manage_mode:
			me = self.get_player(player)
			if self.bankrupter:
				destination = self.get_player(self.bankrupter)

				# Possessions
				for x in self.__board.properties(player):
					self.give_property_to_player(me, x, destination)
				# Money
				destination['money'] += me['money']

				# "Out of Jail" cards
				destination['outjail_comm'] = me['outjail_comm']
				destination['outjail_chance'] = me['outjail_chance']
			else:
				for x in self.__board.properties(player):
					self.remove_property_from_player(me, x)
				del self.players[player]
				self.play_order.remove(player)

			self.emit_pubmsg("\x034 ** Fin de parcours pour %s ! Il déclare banqueroute !\x03" % (player, ))
			self.quit_private(player, None)
			self._next_turn()

	def quit_private(self, player, params):
		self._waiting_for_private = False
		self.manage_mode = False
		self.exchange_mode = False
		self.emit_pubmsg("\x0312 -- Le jeu reprend en mode public ! --\x03")

	def show_status(self, player, params):
		pass

	def free_park(self, player, params):
		self.emit_notice(player, "** Parc Gratuit ** \x02%s %s\x02 **" % (locale.format("%d", self.freepark, True), self.__board.data('currency')))

	def start_exchange(self, player, params):
		me = self.get_player(player)
		if player.lower() == self.play_order[self.active_player].lower() and not me['buyorauc']:
			self.exchange_mode = True
			self._waiting_for_private = True
			self.emit_pubmsg("\x036 -- %s veut faire un échange --\x03" % (player,))
			
			self.list_properties(player, private = True)
			self.emit_privmsg(player, "\x0311 -- Votre patrimoine : \x02%s %s\x02\x03" % (locale.format("%d", me['money'], True), self.__board.data('currency')))
			self.emit_privmsg(player, "\x033 - Tapez !p pour proposer un échange --\x03")
		
	def purpose_exchange(self, player, params):
		if self.exchange_mode and self._waiting_for_private:
			if not self.exchange:
				self.exchange = MonopolyExchange(player)
			if player.lower() == self.exchange.asker().lower() or player.lower() == self.exchange.target().lower():
				if len(params) == 0:
					self.emit_privmsg(player, "\x034[ Echange ] Tapez !p <cible> <liste_echange>\x03")
					self.emit_privmsg(player, "\x033  La liste des échanges est sous la forme 'p<numéro_propriété>' ou 'm<argent>' séparés par une virgule.\x03")
					self.emit_privmsg(player, "\x033  Cette liste se compose des propriétés et/ou argent que vous souhaitez échanger. Vous devrez attendre la proposition de la cible pour continuer.\x03")
					self.emit_privmsg(player, "\x033  \x02Exemple\x02: !p AutreJoueur p15\x03")
				elif len(params) == 1:
					self.emit_privmsg(player, "\x034[ Echange ] Vous devez saisir votre liste d'échange.\x03")
				else:
					target = params[0]
					lst = params[1].split(',')
					if self.exchange.target() is None:
						self.exchange.set_target(target)
					for item in lst:
						self.exchange.add_item(player, item)

					if player.lower() == self.exchange.asker().lower():
						self.emit_privmsg(target, "\x033[ Echange ] %s souhaite échanger des possessions avec vous.\x03" % (self.exchange.asker(),))
						for itm in self.exchange.items(player):
							if itm == 'properties':
								for x in self.exchange.items(player)[itm]:
									prop = self.__board.property(x)
									self.emit_privmsg(target, "\x0312 -- %s -- \x03" % (prop.name(),))
							elif itm == 'money':
								self.emit_privmsg(target, "\x0312 -- %s %s \x03" % (locale.format("%d", self.exchange.items(player)[itm], True), self.__board.data('currency')))
						self.emit_privmsg(target, "\x033 Tapez !p <cible> <list_echange>\x03")
						self.emit_privmsg(target, "\x033 Cette liste se compose des propriétés et/ou argent que vous souhaitez donner en échange de sa proposition.\x03")
					else:
						self.emit_privmsg(self.exchange.asker(), "\x033[ Echange ] %s a répondu à votre proposition.\x03" % (self.exchange.target(), ))
						for itm in self.exchange.items(player):
							if itm == 'properties':
								for x in self.exchange.items(player)[itm]:
									prop = self.__board.property(x)
									self.emit_privmsg(self.exchange.asker(), "\x0312 -- %s -- \x03" % (prop.name(),))
							elif itm == 'money':
								self.emit_privmsg(self.exchange.asker(), "\x0312 -- %s %s \x03" % (locale.format("%d", self.exchange.items(player)[itm], True), self.__board.data('currency')))

						self.exchange.set_waiting(True)
						self.emit_privmsg(self.exchange.asker(), "\x0312 Tapez !a pour accepter, !d pour refuser.\x03")

	def accept_exchange(self, player, params):
		if self.exchange and self._waiting_for_private and self.exchange.waiting() and player == self.exchange.asker():
			asker = self.get_player(self.exchange.asker())
			target = self.get_player(self.exchange.target())
			for p in self.exchange.items(self.exchange.asker())['properties']:
				self.give_property_to_player(asker, self.__board.property(p), target)
			for p in self.exchange.items(self.exchange.target())['properties']:
				self.give_property_to_player(target, self.__board.property(p), asker)
			asker['money'] += self.exchange.items(self.exchange.target())['money']
			target['money'] += self.exchange.items(self.exchange.asker())['money']

			self.emit_privmsg(self.exchange.asker(), "\x033[ Echange validé ]\x03")
			self.emit_privmsg(self.exchange.target(), "\x033[ Echange accepté ]\x03")
			self.terminate_exchange()

	def terminate_exchange(self):
		self.exchange_mode = False
		del self.exchange
		self.quit_private()

	def deny_exchange(self, player, params):
		if self.exchange and self._waiting_for_private and self.exchange.waiting() and player == self.exchange.asker():
			self.emit_privmsg(self.exchange.asker(), "\x034[ Echange annulé ]\x03")
			self.emit_privmsg(self.exchange.target(), "\x034[ Echange refusé ]\x03")
			self.terminate_exchange()
