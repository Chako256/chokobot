#-*- coding:utf-8 -*-

from mybot import module

class WcModule(module.BotModule):
	"""
	Word Counter module

	Counts words, lines and smilies for a user
	Can merge nicknames to get global statistics

	Gets a classification
	"""
	def initialize(self):
		self.config = self.read_conf()

	def cnt(self, nickname, message):
		user = self.data().get_one("stats", nickname=nickname)
		if user is None:
			user = self.data().create_object(
				"stats",
				nickname=nickname,
				words=0,
				lines=0,
				mpl=0
			)
		user.words += len(message.split())
		user.lines += 1
		user.mpl = float(user.words) / float(user.lines)
		self.data().save(user)

	def get_user_stats(self, nickname, channel, *args):
		if len(args) >= 1:
			target = args[0]
		else:
			target = nickname
		user = self.data().get_one("stats", nickname=target)
		if user is not None:
			self.client().privmsg(channel, "\x02..:: %s ::..\x02 \x0312 %d mots\x03,\x032 %d lignes\x03, soit\x034 %.2f mots par ligne\x03." % (
				target,
				user.words,
				user.lines,
				user.mpl
			))
		else:
			self.client().privmsg(channel, "\x034 - Aucune statistique pour \x02{}\x02\x03".format(target))

	def get_top_3(self, nickname, channel, *args):
		top3 = self.data().get(
			"stats",
			sort=[('words', -1)],
			limit=3
		)
		self.client().privmsg(channel,
			"\x02..:: TOP 3 ::..\x02"
		)
		i = 1
		for t in top3:
			self.client().privmsg(channel,
				"\x02{}\x02 - \x032{}\x03 avec\x033 {} mots\x03".format(
					i, t.nickname, t.words
				)
			)
			i += 1

	def merge_nicknames(self, nickname, *args):
		source = self.data().get_one(
			"stats",
			nickname=args[0]
		)
		target = self.data().get_one(
			"stats",
			nickname=args[1]
		)
		if source is not None and target is not None:
			target.words += source.words
			target.lines += source.lines
			target.mpl = float(target.words) / float(target.lines)
			self.data().save(target)
			self.data().delete("stats", _id=source._id)
			self.client().privmsg(nickname,
				"\x02\x033 [Fusion] \x03\x02 \x032{}\x03 a bien été fusionné avec \x034{}\x03.".format(
					source.nickname, target.nickname
				)
			)

	def on_pubmsg(self, serv, ev):
		done_command = False
		nickname = ev.source().nickname
		message = ev.arguments()[0]
		for c in self.config.commands:
			if message.startswith(c):
				mx = message.split()
				mx.pop(0)
				getattr(self, getattr(self.config.commands, c))(nickname, ev.target(), *mx)
				done_command = True
		if not done_command:
			self.cnt(nickname, message)

	def on_pubaction(self, serv, ev):
		nickname = ev.source().nickname
		message = ev.arguments()[0]
		self.cnt(nickname, message)

	def on_privmsg(self, serv, ev):
		nickname = ev.source().nickname
		message = ev.arguments()[0]
		mx = message.split()
		if nickname in self.config.admin.nicks:
			if len(mx) < 3:
				serv.privmsg(nickname, "\x02\x034Usage: !merge <nickname> <target>\x03\x02")
				serv.privmsg(nickname, "\x032 Merges <nickname> stats into <target> ones.\x03")
			else:
				try:
					cmd = getattr(self, getattr(self.config.admin.commands, mx[0]))
					cmd(nickname, *mx[1:])
				except:
					pass
