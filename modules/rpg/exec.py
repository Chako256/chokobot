#-*- coding:utf-8 -*-

from mybot import module
from .components import *
import os
import json
import yaml
from .util import WebApp, RPGException


class RPGModule(module.BotModule):
	"""
	RPG module

	Role playing game on IRC !
	A map is dynamically loaded into the module. This map contains cities and transports.
	Each player can get missions or achievements to earn coins.

	The Coin is the official currency of the BotLand.

	An IRC user must register himself to play.

	The distance between cities is computed by the Pythagorus theorem, between the first point of the left corner of each city.
	Moves duration is so computed with the transport speed and the distance to be covered.
	The transport speed unity is "point per 5 minutes".

	Files in 'map' subfolder are only **fixtures** ! They cannot be considered as current game parameters.
	Once they have been treated, files are renamed with the '.bck' extension to be ignored at each next module start.

	Loans are managed by the BotLand Bank, the interest rate is starting at 7.7%. The maximum loan duration is fixed to 180 days.
	The maximum loan is fixed to C10,000.
	Drafts must be paid every week.

	
	"""
	def __init__(self, name, client):
		super(RPGModule, self).__init__(name, client)
		self.load_fixtures()
		self.config = self.read_conf()

	def load_fixtures(self):
		"""
		Load fixtures for the first time into the database
		When the fixtures are loaded, this method renames the JSON files so they're ignored at the next call
		"""
		fixturedir = os.path.join(os.path.abspath(os.path.dirname(__file__)), "map")
		mapfile = os.path.join(fixturedir, "botland.map.yml")
		transfile = os.path.join(fixturedir, "transports.yml")
		if os.path.isdir(fixturedir) and os.path.isfile(mapfile) and os.path.isfile(transfile):
			_trans = yaml.load(open(transfile, "r"))
			for _tid in _trans:
				_tr = _trans[_tid]
				to = self.data().create_object("transports", cls=RPGTransport,
					tname=_tid,
					**_tr
				)

			_map = yaml.load(open(mapfile, "r"))
			mo = self.data().create_object("maps", cls=RPGMap,
				mapname=_map['name'],
				height=_map['height'],
				width=_map['width'],
				currency=_map['money']['currency'],
				bank=_map['bank']
			)
			mapid = self.data().save(mo)
			for _cname in _map['cities']:
				_city = _map['cities'][_cname]
				co = self.data().create_object("cities", cls=RPGCity,
					cityname=_cname,
					mapid=mapid,
					**_city
				)
				self.data().save(co)

			# os.rename(mapfile, "{}.bck".format(mapfile))
			# os.rename(transfile, "{}.bck".format(transfile))
			self.info("Fixtures loaded.")

	def on_pubmsg(self, server, ev):
		"""
		IRC Event
		"""
		pfx = ev.arguments()[0].split()
		self.debug("Arguments parsed: {}".format(pfx))
		if pfx[0] == "{}rpg".format(self.config.prefix):
			command = None
			try:
				command = pfx[1]
				self.run_command(command, ev.source().nickname, *pfx[2:])
			except RPGException as re:
				self.usage(command, ev.source().nickname)
			except Exception as ex:
				self.error(str(ex))

	def usage(self, command, source):
		"""
		Generic Usage recall in case of error
		"""
		if command is not None:
			self.client().notice(source, self.config.commands.get(command).usage)

	def run_command(self, command, *args):
		"""
		Generic command runner
		"""
		if command in self.config.commands:
			getattr(self, self.config.commands.get(command).callback)(*args)

	def player(self, nickname):
		"""
		Get a player with one of its nicknames
		"""
		return self.data().get_one("players", cls=RPGPlayer, nickname={"$in": nickname})

	def register_player(self, nickname, *args):
		"""
		Register a player into the game
		Each player starts with C0 (no money)

		Arguments:
			[0]: Character name
		"""
		if (len(args) == 0) or (type(args[0]) != str):
			raise RPGException()
		if self.player(nickname) is None:
			po = self.data().create_object("players", cls=RPGPlayer,
				playername=args[0],
				nickname=[nickname],
				money=0,
				inventory=[],
				success=[],
				exploration=0.0
			)
			self.data().save(po)
			self.client().notice(nickname, "\x033\x02Bravo !\x02 Votre personnage est maintenant enregistré sous le nom de \x02{}\x02".format(
				po.playername
			))
		else:
			self.client().notice(nickname, "\x034\x02Erreur:\x02 Vous êtes déjà inscrit !\x03")

	def register_nickname(self, nickname, *args):
		"""
		Register another nickname for a character
		"""
		po = self.data().get("players", cls=RPGPlayer, playername=args[0])
		if po is not None:
			po.add_nickname(nickname)
			self.data().save(po)
			self.client().notice(nickname, "\x033\x02Succès !\x02 Vous avez ajouté le pseudonyme \x02{}\x02 à votre personnage.".format(
				args[0]
			))
		else:
			self.client().notice(nickname, "\x034\x02Erreur:\x02 Vous devez être inscrit pour pouvoir ajouter un pseudonyme !\x03")

	def get_access(self, nickname, *args):
		"""
		Sends a request to the webapp to create a new web user for this nickname
		The user has to already be registered

		[!] This is mandatory to access the webapp
		"""
		po = self.player(nickname)
		if po is None:
			self.client().notice(nickname, "\x034\x02Erreur:\x02 Vous devez être inscrit pour demander l'accès web. Utilisez !rpg register pour vous inscrire !\x03")
		else:
			self.client().notice(nickname, "\x032Veuillez patienter...\x03")
			try:
				_access = WebApp.access(nickname)
				self.client().notice(
					nickname,
					"\x02\x032 Vos accès :\x03\x02 Utilisateur \x02\x0312{}\x03\x02, Mot de passe \x02\x034{}\x03\x02".format(
						_access[0], _access[1]
					)
				)
			except:
				self.client().notice(nickname, "\x034\x02Erreur\x02 lors de la demande d'accès. Veuillez contacter l'administrateur.\x03")

