#-*- coding:utf-8 -*-

from mybot.lib.data import DataObject
import math

class RPGCity(DataObject):
	def get_distance(self, another_city):
		"""
		Pythagorus theorem to compute distance between two cities
		"""
		dx = abs(self.x - another_city.x)
		dy = abs(self.y - another_city.y)
		return math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))

	def can_train(self):
		return self.buildings.train_station > 0

	def can_high_speed_train(self):
		return self.buildings.high_speed_train_station > 0

	def can_boat(self):
		return self.buildings.port > 0

	def can_shop(self):
		return self.buildings.shop > 0
