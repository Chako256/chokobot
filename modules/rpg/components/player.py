#-*- coding:utf-8 -*-

from mybot.lib.data import DataObject

class RPGPlayer(DataObject):
	def add_nickname(self, nickname):
		if not nickname in self.nickname:
			self.nickname.append(nickname)
