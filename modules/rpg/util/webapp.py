#-*- coding:utf-8 -*-

import requests
import json
from .error import WebAppException


class WebApp:
	"""
	Wrapper for requesting web application RPG ChokoBot

	See docs: http://doc.chaksoft.fr/rpgapi.html
	"""
	API_VER = "v1"
	URL = "https://rpg.chaksoft.fr/api/{}/".format(API_VER)
	API_KEY = "9r0gjktd6gwwug43uirhax7r9uyu9sbc"

	@staticmethod
	def access(nickname):
		"""
		Request for creating access to user

		Returns a couple (username, password)
		"""
		_r = requests.post(WebApp.URL, data={
			'nickname': nickname,
			'apikey': WebApp.API_KEY
		})
		if _r.status_code == 200:
			response = _r.json
			return (response['username'], response['password'])
		else:
			raise WebAppException(_r.reason)
