#-*- coding:utf-8 -*-

"""
	Game Library for MyBot
	Configuration Parser and Game Logic
"""

import configparser
import os.path
from . import util
from mybot import module, timer
from datetime import datetime

from threading import Lock
locker = Lock()
"""
	Game Configuration Parser
	
	A INI file must be located in a "conf" subdirectory of your game
"""
class GameConfig:
	def __init__(self, path, id):
		self.cfgfile = configparser.ConfigParser()
		self.cfgfile.read(os.path.join(path, "conf", "%s.ini" % (id,)))
		self.read_data()
		
	def read_data(self):
		self.basecfg = {
			'gamedelay': int(self.cfgfile.get("Base", "GameDelay")),
			'repeatnoplayer': int(self.cfgfile.get("Base", "RepeatNoPlayer")),
			'minplayers': int(self.cfgfile.get("Base", "MinPlayers")),
			'maxplayers': int(self.cfgfile.get("Base", "MaxPlayers")),
			'timelimit': int(self.cfgfile.get("Base", "TimeLimit")),
			'privatetext': self.cfgfile.get("Base", "PrivateText"),
			'cmdprefix': self.cfgfile.get("Base", "Prefix")
		}
		
		self.commands = {}
		self.privatecommands = {}
		
		self.myconfig = {}
		for key in self.cfgfile.options("Config"):
			self.myconfig[key.lower()] = self.cfgfile.get("Config", key)
			if self.myconfig[key.lower()] == "True":
				self.myconfig[key.lower()] = True
			elif self.myconfig[key.lower()] == "False":
				self.myconfig[key.lower()] = False
		
	def bind_command(self, command, callback):
		self.commands[command] = callback
		
	def unbind_command(self, command):
		if command in self.commands:
			del self.commands[command]

	def bind_private_command(self, command, callback):
		self.privatecommands[command] = callback

	def unbind_private_command(self, command, callback):
		self.privatecommands[command] = callback

class Game(module.BotModule):
	def __init__(self, app, client, priority=None, events={}, competmode=False):
		module.BotModule.__init__(self, app, client, priority=priority)
		self.setEnabled(False)
		self.players = {}
		self.play_order = []
		self.active_player = -1
		self.config = None
		self.gameid = ''
		
		self.gamestatitem = None
		self.competmode = competmode
		
		self.sens = 0
		self.total_time = 0
		self.running = False
		self.paused = False	
		
		self.needsregister = True
		self._waiting_for_private = False
		self._is_op = True

	@property
	def channel(self):
	    return self.param('channel')

	def setEnabled(self, flag):
		module.BotModule.setEnabled(self, flag)
		if flag:
			self.reinit()
		else:
			if not self.param('channel') is None:
				self.emit_pubmsg("\x02\x034 ** Jeu terminé. **\x03\x02")
			self.destroy()
		
	def __str__(self):
		return "Game object : id '%s' / channel '%s' / running '%s'" % (self.gameid, self.channel, str(self.running))
		
	def destroy(self):
		if hasattr(self, '_cdown_timer') and self._cdown_timer:
			self._cdown_timer.stop()
		if hasattr(self, '_game_timer') and self._game_timer:
			self._game_timer.stop()
		if hasattr(self, '_waiting_timer') and self._waiting_timer:
			self._waiting_timer.stop()
		
	def reinit(self):
		self.players = {}
		self.total_time = 0
		self.active_player = -1
		self.winner = ""
		self.cdown = self.config.basecfg['gamedelay']
		self.sens = 0
		self._cdown_launched = False
		
		if not self.competmode:
			self._waiting_timer = timer.BotTimer(self.config.basecfg['repeatnoplayer'], self._waiting_proc, ["WaitingTimer"])
			self._waiting_timer.start()
			self._waiting_proc(None)
		
		self._cdown_timer = timer.BotTimer(1, self._game_countdown, ["GameCountDown"])
		self._game_timer = timer.BotTimer(1, self._count_game_time, ["GameTime"])
		
	def _count_game_time(self, args):
		locker.acquire()
		try:
			self.total_time = self.total_time + 1
		finally:
			locker.release()
	
	"""
		"Wait-For-Play" timer callback
	"""
	def _waiting_proc(self, args):
		self.debug("Entering WAITINGPROC")
		try:
			if len(self.players) < self.config.basecfg['minplayers']:
				self.debug(">>> Can join !")
				self.emit_pubmsg('\x02' + " * Inscrivez-vous à " + self.gameid.upper() + " ! Tapez !play si vous souhaitez rejoindre la prochaine partie ! Tapez !help pour l'aide et la liste des commandes disponibles !" + '\x02')
				self.debug(">>> Message emitted.")
				if len(self.players) > 0:
					self.emit_pubmsg(' ** ' + str(len(self.players.keys())) + " joueur(s) inscrit(s) !")
		except Exception as e:
			self.error(str(e))
			self._waiting_timer.stop()
		
	"""
		Countdown to play when minimum players is reached
	"""
	def _game_countdown(self, args):
		if not self.running:
			self.cdown = self.cdown - 1
			if self.cdown == 90:
				self.emit_pubmsg('\x02\x0312' + ' * 90 SECONDES avant le début de la partie !' + '\x03\x02')
			elif self.cdown == 60:
				self.emit_pubmsg('\x02\x032' + ' * 1 MINUTE avant le début de la partie !' + '\x03\x02')
			elif self.cdown == 30:
				self.emit_pubmsg('\x02\x033' + ' * 30 SECONDES avant le début de la partie !' + '\x03\x02')
			elif self.cdown == 10:
				self.emit_pubmsg('\x02\x034' + ' * 10 SECONDES avant le début de la partie !' + '\x03\x02')
			elif self.cdown <= 0:
				self.emit_pubmsg('\x02' + ' * DEBUT DE LA PARTIE * Que le meilleur gagne ! *' + '\x02')
				self.running = True
				self._cdown_timer.stop()
				self._game_timer.start()
				self.start_game()
		else:
			self._cdown_timer.stop()
		
	def get_player(self, nickname):
		if nickname in self.players:
			return self.players[nickname]
		else:
			return None
			
	def pick_previous_player(self, affects = False):
		if self.sens == 0:
			idx = (self.active_player - 1 + (len(self.play_order) * 20)) % len(self.play_order)
		else:
			idx = (self.active_player + 1 + (len(self.play_order) * 20)) % len(self.play_order)
		if affects:
			self.active_player = idx
		return self.play_order[idx]
			
	def next_player(self, reverse = False):
		if not reverse:
			if self.sens == 0:
				self.active_player = (self.active_player + 1 + (len(self.play_order) * 20)) % len(self.play_order)
			else:
				self.active_player = (self.active_player - 1 + (len(self.play_order) * 20)) % len(self.play_order)
		else:
			if self.sens == 1:
				self.active_player = (self.active_player + 1 + (len(self.play_order) * 20)) % len(self.play_order)
			else:
				self.active_player = (self.active_player - 1 + (len(self.play_order) * 20)) % len(self.play_order)
		return self.play_order[self.active_player]
		
	def register_player(self, nickname, params, message=None):
		try:
			if not self.running:
				if not nickname in self.players:
					if len(self.players) < self.config.basecfg['maxplayers']:
						self.players[nickname] = {}
						self.play_order.append(nickname)
						if message is None:
							message = '\x0312* %s est inscrit(e) pour la prochaine partie !\x03' % (nickname, )
						self.emit_pubmsg(message)
							
						if len(self.players) == self.config.basecfg['minplayers']:
							self._waiting_timer.stop()
							self.emit_pubmsg(' ** ' + str(self.config.basecfg['minplayers']) + ' joueurs inscrits ! La partie commence dans ' + str(self.config.basecfg['gamedelay']) + ' secondes !')
							self._cdown_launched = True
							self._cdown_timer.start()
					else:
						self.emit_notice(nickname, "** Le nombre maximum de joueurs pour cette partie a été atteint. Merci d'attendre la prochaine partie :(")
				else:
					self.emit_notice(nickname, "\x02* Vous êtes déjà enregistré(e) pour la prochaine partie !\x02")
			else:
				self.emit_notice(nickname, "\x02* Vous n'êtes pas autorisé(e) à vous inscrire pour la prochaine partie !\x02")
		except Exception as ex:
			self.error(repr(ex))
			raise ex

	def unregister_player(self, nickname, params):
		if nickname in self.players:
			if nickname in self.play_order:
				self.play_order.remove(nickname)
			del self.players[nickname]
			if not self.running:
				self.emit_pubmsg("\x034* %s n'est plus inscrit(e) pour la prochaine partie !\x03" % (nickname, ))
			else:
				self.emit_pubmsg("\x034 [ Départ ] %s a quitté la partie !\x03" % (nickname, ))
			
			if len(self.players) < self.config.basecfg['minplayers']:
				self.emit_pubmsg('\x034' + "* Partie terminée !" + '\x03')
				self.stop_game()
			else:
				self._next_turn()

	def on_pubmsg(self, server, event):
		if event.arguments()[0].strip() != '':
			if self.param('channel') is None or event.target().lower() == self.param('channel').lower():
				commands = event.arguments()[0].split()
				self.parse_command('public', event.source().nickname, commands[0], [] if len(commands) == 1 else commands[1:])

	def on_kick(self, server, event):
		if self.param('channel') is None or event.target().lower() == self.param('channel').lower():
			commands = event.arguments()[0].split()
			self.parse_command('kick', event.source().nickname, commands[0], [] if len(commands) == 1 else commands[1:])

	def on_part(self, server, event):
		if self.param('channel') is None or event.target().lower() == self.param('channel').lower():
			commands = event.arguments()[0].split()
			self.parse_command('part', event.source().nickname, commands[0], [] if len(commands) == 1 else commands[1:])

	def on_quit(self, server, event):
		commands = event.arguments()[0].split()
		self.parse_command('quit', event.source().nickname, commands[0], [] if len(commands) == 1 else commands[1:])

	def on_nick(self, server, event):
		commands = event.arguments()[0].split()
		self.parse_command('nick', event.source().nickname, commands[0], [] if len(commands) == 1 else commands[1:])

	def on_error(self, server, event):
		"""
		Error handling -> disconnection from the game

		@since 5.4
		"""
		commands = event.arguments()[0].split()
		self.parse_command('quit', event.source().nickname, commands[0], [] if len(commands) == 1 else commands[1:])

	def on_join(self, server, event):
		"""
		Automatic voice on join

		@since 5.0
		"""
		if event.source().nickname != server.nickname():
			server.mode(event.target(), '+v {}'.format(event.source().nickname))
				
	def parse_command(self, type, source, command, params):
		self.debug("[Game] Parsing command %s : %s : %s : %s" % (type, source, command, params))
		if type == 'public' and self._waiting_for_private and command.startswith(self.config.basecfg['cmdprefix']):
			self.emit_notice(source, self.config.basecfg['privatetext'])
		elif type == 'nick':
			if source in self.players:
				self.players[command] = self.players[source]
				del self.players[source]
				
				if source in self.play_order:
					idx = self.play_order.index(source)
					self.play_order[idx] = command
				
				self.emit_pubmsg("\x0312 [ Changement de pseudonyme ] %s devient %s !\x03" % (source, command))
		elif type == 'part' or type == 'quit':
			if source in self.players:
				self.emit_pubmsg("\x034 [ Départ ] %s a quitté la partie !\x03" % (source, ))
				self.active_player = self.active_player - 1
				self.player_defects(source, params)
				del self.players[source]
				self.play_order.remove(source)

				if len(self.players) < self.config.basecfg['minplayers']:
					self.emit_pubmsg("\x02\x034 ** FIN DE PARTIE ** Partie annulée par manque de joueurs !**\x03\x02")
					self.stop_game()
				else:
					self._next_turn()
		elif type == 'kick':
			kicked = command
			if kicked in self.players:
				self.emit_pubmsg("\x034 [ Départ ] %s a quitté la partie !\x03" % (kicked, ))
				self.active_player = self.active_player - 1
				self.player_defects(kicked, params)
				del self.players[kicked]
				self.play_order.remove(kicked)
				
				if len(self.players) < self.config.basecfg['minplayers']:
					self.emit_pubmsg("\x02\x034 ** FIN DE PARTIE ** Partie annulée par manque de joueurs ! **\x03\x02")
					self.stop_game()
				else:
					self._next_turn()

		if not command is None and type == 'public' and not self._waiting_for_private and command.startswith(self.config.basecfg['cmdprefix']):
			if (not source in self.players and self.needsregister) and self.running and type == 'public':
				self.emit_notice(source, "* Vous n'êtes pas inscrit pour cette partie ! O_o")
			else:
				self.run_callback(type, source, command, params)
		elif (not command is None and type == 'private' and self._waiting_for_private and command.startswith(self.config.basecfg['cmdprefix'])):
			if (not source in self.players and self.needsregister) and self.running:
				self.emit_notice(source, "* Vous n'êtes pas inscrit pour cette partie ! O_o")
			else:
				self.run_callback(type, source, command, params)

	def on_privmsg(self, server, event):
		commands = event.arguments()[0].split()
		self.parse_command('private', event.source().nickname, commands[0], [] if len(commands) == 1 else commands[1:])
					
	def run_callback(self, type, source, command, params):
		if command.startswith(self.config.basecfg['cmdprefix']):
			if type != 'private':
				cbk = self.config.commands.get(command.lower())
			else:
				cbk = self.config.privatecommands.get(command.lower())
			if not cbk is None:
				if (self.paused and (command == self.config.basecfg['cmdprefix'] + self.config.myconfig['pausecommand']) or (command == self.config.basecfg['cmdprefix'] + self.config.myconfig['resumecommand'])) or (not self.paused) or (command == '!time'):
					cbk(source, params)
				else:
					self.emit_notice(source, "\x034 * Le jeu est actuellement en pause !\x03")
			else:
				self.emit_notice(source, "* Commande " + command + " inconnue")
				
	"""
		Force the game to start instantly without the Game timer delay
	"""
	def force_start(self, nickname, params):
		if nickname in self.players and self._cdown_launched:
			if not self.running:
				self.cdown = 1
				self.emit_pubmsg('\x034 * ' + "Démarrage imminent de la partie !" + '\x03')
				
	def _next_turn(self):
		pass
				
	def start_game(self, **extradata):
		# self.mute_channel()
		self.gamestatitem = self.data().create_object(
			self.gameid,
			gamedate=datetime.now(),
			gameduration=0,
			gamewinner="",
			gamewintime=0,
			channel=self.channel,
			**extradata
		)

	def mute_channel(self):
		"""
		Mutes the channel and allows the players only to speak

		@since 5.0
		"""
		if self._is_op:
			self.client().mode(self.channel, '+m')
			for nick in self.client().get_user_list(self.channel):
				self.client().mode(self.channel, '-v {}'.format(nick))
			for nick in self.players:
				self.client().mode(self.channel, '+v {}'.format(nick))

	def unmute_channel(self):
		"""
		Unmutes the channel and allows everyone to speak again

		@since 5.0
		"""
		if self._is_op:
			self.client().mode(self.channel, '-m')
			for nick in self.client().get_user_list(self.channel):
				self.client().mode(self.channel, '+v {}'.format(nick))
		
	def save_statitem(self):
		self.data().save(self.gamestatitem)
		
	def stop_game(self):
		"""
		Stop game after is has been started
		Save data if exists and unmute channel if the game has been started.
		"""
		if self.gamestatitem is not None and self.gamestatitem.gamewinner != "":
			self.save_statitem()
			# self.unmute_channel()
		self._cdown_launched = False
		self._cdown_timer.stop()
		self._game_timer.stop()
		if not self.competmode:
			self.reinit()
		
	def player_defects(self, player, params):
		pass
		
	def emit_pubmsg(self, message):
		self.client().privmsg(self.param('channel'), message)
			
	def emit_notice(self, target, message):
		self.client().notice(target, message)
			
	def emit_privmsg(self, target, message):
		self.client().privmsg(target, message)
			
	def show_time(self, player, params):
		if self.running:
			s = "* La partie est démarrée depuis " + util.seconds_to_string_time(self.total_time)
			if self.paused:
				s = s + ". La partie est actuellement en PAUSE."
			self.emit_notice(player, s)
		else:
			self.emit_notice(player, "\x034* Aucune partie n'est en cours.\x03")
			
	def pause_game(self, player, params):
		if player in self.players:
			if self.paused:
				self.emit_pubmsg("\x02 * La partie reprend !\x02")
				self.paused = False
				self._game_timer.start()
			else:
				self.emit_pubmsg("\x02 * PAUSE *\x02")
				self.paused = True
				self._game_timer.stop()
				
	def show_myrecord(self, player, params):
		nick = player
		if len(params) >= 1:
			nick = params[0]
		self.emit_notice(player, "\x02 * STATISTIQUES DE " + nick + " *\x02")
		rec = self.data().get_min(self.gameid, "gameduration", gamewinner=nick)
		if not rec is None:
			self.emit_notice(player, "* RECORD PERSONNEL DE VICTOIRE : \x034Sur " + rec.channel + "\x03 \x033Le " + rec.gamedate + "\x03 \x032 en " + util.seconds_to_string_time(rec.gamewintime) + "\x03")
			rec2 = self.data().get_count(self.gameid, gamewinner=nick)
			self.emit_notice(player, "* NOMBRE DE VICTOIRES :\x032 " + str(rec2) + " \x03")
		else:
			self.emit_notice(player, "* \x034AUCUNE VICTOIRE ENREGISTREE POUR " + nick + "\x03")
			
	def show_records(self, player, params):
		rec = self.data().get_min(self.gameid, "gameduration")
		win = self.data().get_min(self.gameid, "gamewintime")
		slow = self.data().get_max(self.gameid, "gameduration")
		if not rec is None and not win is None and not slow is None:
			self.emit_notice(player, "* RECORD ABSOLU DE PARTIE : \x034Sur " + rec.channel + "\x03 \x033Le " + rec.gamedate + "\x03 \x032en " + util.seconds_to_string_time(rec.gameduration) + "\x03")
			self.emit_notice(player, "* RECORD ABSOLU DE VICTOIRE : \x034Sur " + win.channel + "\x03 \x033Le " + win.gamedate + "\x03 par " + win.gamewinner + " \x032en " + util.seconds_to_string_time(win.gameduration) + "\x03")
			self.emit_notice(player, "* RECORD DE LA PARTIE LA PLUS LONGUE : \x034Sur " + slow.channel + "\x03 \x033Le " + slow.gamedate + "\x03 \x032en " + util.seconds_to_string_time(slow.gameduration) + "\x03")
		else:
			self.emit_notice(player, "* AUCUN RECORD ENREGISTRE POUR LE MOMENT")

	def on_chanoprivsneeded(self, server, ev):
		self._is_op = False
		
