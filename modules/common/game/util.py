#-*- coding:utf-8 -*-

def seconds_to_string_time(seconds):
	"""
	Transform a number of seconds into string format:
		XX hours XX minutes XX seconds
	"""
	hr = int(seconds / 3600)
	mn = int(seconds / 60) - (hr * 60)
	sec = seconds - (mn * 60) - (hr * 3600)
	result = ""
	if hr > 0:
		result = result + str(hr) + " heure(s) "
	if mn > 0:
		result = result + str(mn) + " minute(s) "
	result = result + str(sec) + " seconde(s)"
	return result

