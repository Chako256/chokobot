#-*- coding:utf-8 -*-

# Pendu Game

import string
from modules.common.game import game
from modules.common.game import util
import os.path
from mybot import timer

class PenduGame(game.Game):
	def __init__(self, name, client, priority=None, events={}):
		game.Game.__init__(self, name, client, priority=priority, events=events)
		self.gameid = 'pendu'
		self.config = game.GameConfig(os.path.dirname(os.path.abspath(__file__)), 'pendu')
		
		for key in self.config.cfgfile.options("Commands"):
			self.config.commands[self.config.basecfg['cmdprefix']+key] = getattr(self, self.config.cfgfile.get("Commands", key))
		for key in self.config.cfgfile.options("PrivateCommands"):
			self.config.privatecommands[self.config.basecfg['cmdprefix']+key] = getattr(self, self.config.cfgfile.get("PrivateCommands", key))
		self.setEnabled(False)
		
		self.needsregister = False
		self._waiting_for_private = True
		
	def destroy(self):
		if hasattr(self, 'gtimer'):
			if self.gtimer:
				self.gtimer.stop()
		
	def reinit(self):
		self.current_word = ''
		self.category = ''
		self.purposer = ''
		self.word_letters = []
		self.remaining_tries = 10
		self.letters_tried = []
		self.running = False
		self._waiting_for_private = True
		self.timedown = int(self.config.myconfig['delaytofindword'])
		
		self.gtimer = timer.BotTimer(1, self._count_down, ["CountDown"])
		
	def _count_down(self, args):
		self.timedown = self.timedown - 1
		if self.timedown == 120:
			self.emit_pubmsg("\x034 * 2 minutes restantes *\x03")
		elif self.timedown == 60:
			self.emit_pubmsg("\x034 * 1 minute restante *\x03")
		elif self.timedown <= 0:
			self.destroy()
			self.emit_pubmsg("\x034 * Délai dépassé ! Le mot était : \x02" + self.current_word + " \x02*\x03")
			self.reinit()

	def on_privmsg(self, server, event):
		if self.enabled():
			self.parse_command('private', event.source().nickname, event.arguments()[0].split()[0], event.arguments()[0].split()[1:])
	
	def submit_word(self, player, params):
		if self.current_word == '' and len(params) > 0:
			word = ' '.join(params[1:]).upper()
			self.category = params[0]
			if len(word) <= 3:
				self.emit_notice(player, "\x034 [ Mot trop court ] Merci de choisir un mot de plus de 3 lettres.\x03")
			else:
				self.current_word = word
				
				for lt in self.current_word:
					if lt in string.ascii_letters[26:]:
						self.word_letters.append([lt, False])
					else:
						self.word_letters.append([lt, True])
				if self.config.myconfig['firstletter']:
					self.try_letter(None, [self.current_word[0]])
				if self.config.myconfig['lastletter']:
					self.try_letter(None, [self.current_word[-1]])
				
				self.running = True
				self._waiting_for_private = False
				self.emit_pubmsg("\x02 * " + player + " a proposé un mot ! Vous avez 3 minutes pour le trouver !\x02")
				self.purposer = player
				self.display_word()
				self.gtimer.start()
		elif len(params) == 0:
			self.emit_notice(player, "\x033 * Usage: !submit <catégorie> <proposition>\x03")
		elif self.current_word != '':
			self.emit_privmsg(player, "\x034 * Un mot a déjà été soumis !\x03")
			
	def try_letter(self, player, params):
		if player != self.purposer:
			if self.current_word != '':
				if len(params) > 0:
					letter = params[0].upper()
					if not letter in self.letters_tried:
						if not player is None:
							self.emit_pubmsg("\x0312 * " + player + " demande la lettre \x02" + letter + "\x02 !\x03")
						if letter in self.current_word:
							for n in self.word_letters:
								if n[0] == letter:
									n[1] = True
							self.check_word_discovered(player)
						else:
							self.fail_try()
							
						self.letters_tried.append(letter)
					else:
						self.emit_pubmsg("\x02\x034 * La lettre " + letter + " a déjà été essayée !\x03\x02")
					if not player is None:
						self.display_word()
			else:
				self.emit_notice(player, "\x034 * Aucun mot n'a été proposé !\x03")
		else:
			self.emit_notice(player, "\x034 * Vous ne pouvez pas jouer votre propre mot !\x03")
			
	def fail_try(self):
		self.remaining_tries = self.remaining_tries - 1
		if self.remaining_tries > 0:
			self.emit_pubmsg("\x02\x033 * " + str(self.remaining_tries) + " essais restants.\x03\x02")
		else:
			self.emit_pubmsg("\x034 * Trop d'erreurs tue l'erreur ! Le mot à découvrir était : \x02" + self.current_word + " \x02*\x03")
			self.destroy()
			self.reinit()
			
	def purpose_word(self, player, params):
		if player != self.purposer:
			if self.current_word != '':
				if len(params) > 0:
					word = ' '.join(params[:]).upper()
					self.emit_pubmsg("\x0310 * " + player + " propose le mot \x02" + word + "\x02 !\03")
					if word != self.current_word:
						self.emit_pubmsg("\x034 * Mauvaise réponse !\x03")
						self.fail_try()
						self.display_word()
					else:
						self.word_found(player)
				else:
					self.emit_notice(player, "\x033 * Usage: !word <mot>\x03")
			else:
				self.emit_notice(player, "\x034 * Aucun mot n'a été proposé !\x03")
		else:
			self.emit_notice(player, "\x034 * Vous ne pouvez pas jouer votre propre mot !\x03")
	
	def display_word(self):
		line = '';
		if self.category != '':
			line = line + '\x033[ ' + self.category + ' ] '
		for n in self.word_letters:
			if n[1]:
				line = line + n[0] + " "
				if n[0] == ' ':
					line = line + "  "
			else:
				line = line + "_ "
		self.emit_pubmsg(line)
				
	def check_word_discovered(self, player):
		discovered = True
		for n in self.word_letters:
			if not n[1]:
				discovered = False
		if discovered:
			self.word_found(player)
			
	def word_found(self, player):
		self.emit_pubmsg("\x02\x0310 * Félicitations, " + player + " ! Vous avez trouvé ! La réponse était bien : " + self.current_word + ".\x03\x02")
		"""try:
			self.save_statitem()
		except Exception as e:
			print(str(e))"""
		self.destroy()
		self.reinit()
		
	def show_help(self, player, params):
		self.emit_privmsg(player, "\x02 * Aide du module de jeu PENDU *\x02")
		self.emit_privmsg(player, " Chaque utilisateur peut soumettre un mot au bot, en message privé en tapant \x0312 !submit <catégorie> <mot / phrase>\x03.")
		self.emit_privmsg(player, "\x034 ATTENTION ! Le <mot> ne peut pas contenir d'accents !!\x03")
		self.emit_privmsg(player, " Une fois le mot saisi, le bot déclenche en mode public le comportement du Pendu.")
		self.emit_privmsg(player, " Tous les utilisateurs du canal peuvent chercher ce mot en proposant des lettres avec \x0312 !letter <proposition>\x03.")
		self.emit_privmsg(player, " Quand un utilisateur veut proposer un mot, il peut taper \x0312 !word <proposition>\x03.")
		self.emit_privmsg(player, " Les joueurs disposent de " + str(self.config.myconfig['delaytofindword']) + " secondes pour trouver le mot proposé.")
