#-*- coding:utf-8 -*-

import random
import configparser
import urllib.request
import time
import os.path
from datetime import datetime, timedelta
import locale

from mybot import module

locale.setlocale(locale.LC_ALL, '')

""" Jokes Module
	For joke parsing from a message
"""	

LOCAL_JOKE_DIR = os.path.dirname(os.path.abspath(__file__))
		
class JokeManager(module.BotModule):
	def __init__(self, name, client, priority=None):
		module.BotModule.__init__(self, name, client, priority=priority)
		jokefile = configparser.ConfigParser()
		jokefile.read(os.path.join(LOCAL_JOKE_DIR, "conf", "joke.ini"), encoding="utf-8")
		
		self.jokeprefix = jokefile.get("Jokes", "Prefix")
		self.listcommand = jokefile.get("Jokes", "ListCommand")
		
		self.debug("[Joker] JokePrefix: " + self.jokeprefix)
		self.debug("[Joker] ListCommand: " + self.listcommand)
		
		self.allowall = jokefile.get("Jokes", "AllowAll") == "True"
		
		self.variables = {}
		for jk in jokefile["Variables"]:
			value = jokefile.get("Variables", jk).split(":")
			self.variables[jk.lower()] = { 'type': value[0], 'value': ":".join(value[1:]) }
		self.debug("[Joker] Variables loaded.")
		
		self.noparam = {}
		for jk in jokefile["NoParamJokes"]:
			elem = jokefile.get("NoParamJokes", jk)
			if elem.startswith('`') and elem.endswith('`') and elem[1:-1].lower() in self.variables:
				if self.variables[elem.lower()[1:-1]]['type'] == 'file':
					self.noparam[jk] = ['LIST']
					persofile = configparser.ConfigParser()
					persofile.read(os.path.join(LOCAL_JOKE_DIR, "conf", self.variables[elem.lower()[1:-1]]['value']), encoding="utf-8")
					
					for psjk in persofile.options("NoParamJokes"):
						self.noparam[jk].append(persofile.get("NoParamJokes", psjk))
				elif self.variables[elem.lower()[1:-1]]['type'] == 'proc':
					self.noparam[jk] = ['PROC', getattr(self, self.variables[elem.lower()[1:-1]]['value'])]
			else:
				self.noparam[jk] = elem
				
		self.debug("[Joker] NoParam jokes loaded.")
			
		self.param = {}
		for jk in jokefile["ParamJokes"]:
			elem = jokefile.get("ParamJokes", jk)
			if elem.startswith('`') and elem.endswith('`') and elem[1:-1].lower() in self.variables:
				if self.variables[elem.lower()[1:-1]]['type'] == 'file':
					self.param[jk] = ['LIST']
					persofile = configparser.ConfigParser()
					persofile.read(os.path.join(LOCAL_JOKE_DIR, "conf", self.variables[elem.lower()[1:-1]]['value']), encoding="utf-8")
					
					for psjk in persofile.options("ParamJokes"):
						self.param[jk].append(persofile.get("ParamJokes", psjk))
				elif self.variables[elem.lower()[1:-1]]['type'] == 'proc':
					self.param[jk] = ['PROC', getattr(self, self.variables[elem.lower()[1:-1]]['value'])]
			else:
				self.param[jk] = elem
				
		self.selfjokes = {}
		for jk in jokefile["SelfJokes"]:
			elem = jokefile.get("SelfJokes", jk)
			self.selfjokes[jk.lower()] = elem
			
		self.debug("[Joker] Self and Param jokes loaded.")
			
		self.properties = {}
		
	def add_property(self, name, value):
		self.properties[name] = value
		
	def del_property(self, name):
		if name in self.properties:
			del self.properties[name]
			
	def get_property(self, name):
		if name in self.properties:
			return self.properties[name]
		else:
			return None
			
	"""
		Build sentence for "%%" magic token
	"""
	def _get_many_params(self, list, exclude = []):
		result = ""
		if len(list) == 1:
			result = list[0]
		else:
			for i in range(len(list)):
				if not list[i] in exclude:
					result = result + list[i]
					if i < len(list) - 2:
						 result = result + ", "
					elif i == len(list) - 2:
						result = result + " et "
		return result
		
	"""
		Get Remote Sentence
	"""
	def get_motive_moi(self, sender, args = None):
		try:
			url = "http://www.motive-moi.fr/irc.php"
			if not args is None:
				number = args[0]
				if not number is None:
					url = url + "?citation=" + number
			s = urllib.request.urlopen(url)
			used_charset = s.getheader("Content-Type").split(';')[1].split('=')[1]
			res = str(s.read(), used_charset)
			s.close()
			
			d = res[:-2].split('|') # There is a \r\n at the end of the stream
			res = d[0] + " (" + d[1] + ")"

			res = res.replace("&agrave;", "à")
			res = res.replace("&eacute;", "é")
			res = res.replace("&egrave;", "è")
			res = res.replace("&ecirc;", "ê")
			res = res.replace("&euml;", "ë")
			res = res.replace("&auml;", "ä")
			res = res.replace("&icirc;", "î")
			res = res.replace("&iuml;", "ï")
			res = res.replace("&ugrave;", "ù")
			res = res.replace("&ouml;", "ö")
			res = res.replace("&ocirc;", "ô")
			
			return res
		except Exception as e:
			return "* Erreur: " + str(e)
			
	def get_today(self, sender, params = None):
		today = datetime.now()
		if params:
			today += timedelta(days = params)
		xf = configparser.ConfigParser()
		xf.read(os.path.join(LOCAL_JOKE_DIR, "conf", "today.ini"), encoding="utf-8")
		raw = xf.get('NoParamJokes', today.strftime("D%dM%m"))
		data = raw.split(';')
		almanach = today.strftime("%A %d %B %Y")
		if len(data) > 2:
			zdays = [ z.strip() for z in data[2].split('/') ]
		else:
			zdays = []
		saint = "Nous fêtons la \x02" + data[1] + "\x02"
		return (str("\x02") + "* " + almanach + " *" + str("\x02") + "\r\n" + saint + "\r\n" +  ("\r\n".join([ ("    \x0312" + z + "\x03") for z in zdays ])))

	def get_tomorrow(self, sender, params = None):
		return self.get_today(sender, 1)

	def get_yesterday(self, sender, params = None):
		return self.get_today(sender, -1)
		
	def get_random_love(self, sender, params):
		if params is None or len(params) == 0:
			return None
		else:
			dest = params[0]
			if len(params) > 1:
				sd = params[1]
			else:
				sd = sender
			
			now = time.localtime()
			day = now.tm_mday
			month = now.tm_mon
			year = now.tm_year
			
			sender_value = 0
			for c in sd:
				sender_value = sender_value + ord(c)
			dest_value = 0
			for d in dest:
				dest_value = dest_value + ord(d)
			
			pct = ((day - month + year) * (sender_value + (2 * dest_value))) % 100
						
			result = "[ <3" + ''.join(['3' for i in range(int(pct/10))]) + " ] Compatibilité entre " + sd + " et " + dest + " : " + str(int(pct)) + " % [ <3" + ''.join(['3' for i in range(int(pct/10))]) + " ]"
			return result

	# Public message event
	def on_pubmsg(self, serv, ev):
		rsx = self.parse_joke(ev.source().nickname, ev.arguments()[0], serv.get_user_list(ev.target()))
		if not rsx is None:
			rslist = rsx.split("\r\n")
			for rs in rslist:
				serv.privmsg(ev.target(), rs)
			
	"""
		Main method for joke parsing
	"""
	def parse_joke(self, sender, message, userlist = None):
		result = None
		if len(message.strip()) > 0:
			trigger = message.split()[0]
			if trigger.lower()[1:] in self.selfjokes:
				if trigger.startswith(self.jokeprefix):
					result = self.selfjokes[trigger.lower()[1:]]
			elif trigger != self.listcommand:
				params = message.split()[1:]
				if trigger.startswith(self.jokeprefix):
					# Cut the prefix
					idtrig = trigger[(len(self.jokeprefix)):].lower()
					if (params is None) or (len(params) == 0):
						joke = self.noparam.get(idtrig)
						if not joke is None:
							result = joke
							if joke[0] == 'LIST':
								rd = random.randrange(start=0, stop=2000)
								rd = rd % (len(joke) - 1)
								result = joke[rd+1]
							elif joke[0] == 'PROC':
								result = joke[1](sender)
								
							result = result.replace('$$_ME', sender)
							if result.find('$$_ALL') >= 0:
								if not self.allowall:
									result = None
								else:
									result = result.replace('$$_ALL', self._get_many_params(userlist, exclude = [sender]))
					else:
						joke = self.param.get(idtrig)
						if not joke is None:
							result = joke
							if joke[0] == 'LIST':
								rd = random.randrange(start=0, stop=999999)
								rd = rd % (len(joke) - 1)
								result = joke[rd+1]
							elif joke[0] == 'PROC':
								result = joke[1](sender, params)
								
							result = result.replace('$$_ME', sender)
							for i in range(len(params)):
								result = result.replace('$$_'+str(i+1), params[i])
							result = result.replace('$$_@', self._get_many_params(params))							
							
			else:
				list = ["* Liste des commandes *"]
				for np in self.noparam:
					list.append(np)
				for p in self.param:
					list.append(p)

				self.client().notice(sender, list[0])
				idx = 1
				line = ""
				total = ""
				while idx < len(list)-1:
					if not list[idx] in total:
						line = line + list[idx] + " / "
						total = total + line
						if idx%8 == 0:
							self.client().notice(sender, line)
							line = ""
					idx = idx + 1
				if len(line) > 0:
					self.client().notice(sender, line)
					
		return result
