#-*- coding:utf-8 -*-

from mybot import module
from datetime import datetime, timedelta


class IgnoreModule(module.BotModule):
	"""
	Ignorance module.

	This module saves and ignorance list,
	where users cannot interact anymore with the bot.
	This ignorance is time limited, and limited to administrators.

	Commands must be sent to the bot as PRIVATE MESSAGES.
	"""
	def __init__(self, name, client, priority=None):
		super(IgnoreModule, self).__init__(name, client, priority=priority)
		self.config = self.read_conf()

	def on_privmsg(self, serv, ev):
		"""
		Private message listener.

		Every command must be :
			!<command> <nickname> <duration>
		The duration is in minutes.
		"""
		if ev.source().nickname in self.config.admins:
			msg = ev.arguments()[0].split()
			command = msg[0]
			if command.startswith(self.config.prefix):
				callback = self.config.commands.get(command[1:])
				if callback is not None:
					getattr(self, callback)(ev, serv, msg[1:])
		else:
			return self.check_ignore(ev.source().nickname)

	def on_pubmsg(self, serv, ev):
		return self.check_ignore(ev.source().nickname)

	def on_pubnotice(self, serv, ev):
		return self.check_ignore(ev.source().nickname)

	def on_nick(self, serv, ev):
		"""
		When a user change his nickname,
		the module must check if he's been ignored.
		If yes, the module change the nickname in the database.
		"""
		_ign = self.data().get_one("ignore", nickname=ev.source().nickname)
		if _ign is not None:
			if len(ev.arguments()) > 0:
				_ign.nickname = ev.arguments()[0]
			else:
				_ign.nickname = ev.target()
			self.data().save(_ign)

	def on_kick(self, serv, ev):
		"""
		Event on kick
		If the bot is kicked, the user is automatically ignored.
		"""
		if ev.target() == serv.nickname():
			if self.check_ignore(ev.source().nickname, current=False):
				minutes = 30*24*60
			else:
				minutes = 24*60
			_ign = self.add_ignore(ev.source().nickname, minutes)
			for admin in self.config.admins:
				self.notify_admin(
					admin,
					"\x034\x02[ALERTE]\x02 Le robot a été kické. Il ignorera \x02{}\x02 jusqu'au \x02{}\x02\x03".format(
						_ign.nickname,
						_ign.until.strftime("%d/%m/%Y %H:%M:%S")
					)
				)

	def check_ignore(self, nickname, current=True):
		if not current:
			_ign = self.data().get_one("ignore", nickname=nickname)
		else:
			_ign = self.data().get_one("ignore", nickname=nickname, until={"$gt": datetime.now()})
		if _ign is not None:
			return False
		return True

	def notify_admin(self, admin, text):
		self.client().notice(admin, text)

	def add_ignore(self, nickname, minutes, admin=None):
		"""
		Adds an user to the ignorance list
		"""
		_ign = self.data().get_one("ignore", nickname=nickname)
		if _ign is not None:
			_ign.until = _ign.until + timedelta(minutes=minutes)
		else:
			_ign = self.data().create_object(
				"ignore",
				nickname=nickname,
				until=datetime.now() + timedelta(minutes=minutes)
			)
		if admin is not None:
			self.notify_admin(admin, "\x033\x02{}\x02 sera ignoré jusqu'à \x02{}\x02\x03".format(
				_ign.nickname,
				_ign.until.strftime("%d/%m/%Y %H:%M:%S")
			))
		self.data().save(_ign)
		return _ign

	def ignore_user(self, ev, serv, args):
		self.add_ignore(ev.source().nickname,
			args[0],
			int(args[1]))

	def unignore_user(self, ev, serv, args):
		"""
		Removes an user from the ignorance list
		"""
		_ign = self.data().get_one("ignore", nickname=args[0])
		if _ign is not None:
			self.data().delete("ignore", nickname=_ign.nickname)
			serv.notice(ev.source().nickname, "\x032\x02{} n'est plus ignoré.\x03".format(
				_ign.nickname
			))

	def list_ignore(self, ev, serv, args):
		"""
		List all the ignores
		"""
		_igns = self.data().get("ignore")
		_list = ["** Liste d'ignorance **"]
		for _ign in _igns:
			_list.append(" - {} ignoré jusqu'au : {}".format(
				_ign.nickname,
				_ign.until.strftime("%d/%m/%Y %H:%M:%S")
			))
		for _l in _list:
			serv.privmsg(ev.source().nickname, _l)
