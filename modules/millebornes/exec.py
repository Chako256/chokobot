#-*- coding:utf-8 -*-

# MilleBornes Game Module for ChokoBot v5

import random
from modules.common.game import game
from modules.common.game import util
import os.path
from datetime import datetime

ATTACKS = { "panne": [ "Panne d'essence", "pe" ], "accident": [ "Accident", "ac" ], "crevaison": [ "Crevaison", "cv" ], "limite": [ "Limitation de vitesse", "lv" ], "feu_rouge": [ "Feu Rouge", "fr" ] }
DEFENSE = { "repare": [ "Réparations", "rp" ], "essence": [ "Essence", "es" ], "roue_secours": [ "Roue de Secours", "rs" ], "fin_limite": [ "Fin de limitation de vitesse", "fl" ], "feu_vert": [ "Feu Vert", "fv" ] }
JOKERS =  { "as_volant": [ "As du Volant", "av" ], "citerne": [ "Citerne d'essence", "ce" ], "increvable": [ "Increvable", "iv" ], "prioritaire": [ "Véhicule prioritaire", "vp" ] }
MILES =   { "25": "25 km", "50": "50 km", "75": "75 km", "100": "100 km", "200": "200 km" }

REFUEL_NAMES = [ 'Esso', 'Total', 'BP', 'Shell', 'Agip' ]

class MilleBornesGame(game.Game):
	def __init__(self, name, client, priority=None, events={}):
		game.Game.__init__(self, name, client, priority=priority)
		self.gameid = 'millebornes'
		self.config = game.GameConfig(os.path.dirname(os.path.abspath(__file__)), 'millebornes')
		
		# Must be repeated on each Game Object because of the <getattr> method.
		for key in self.config.cfgfile["Commands"]:
			self.config.commands[self.config.basecfg['cmdprefix']+key] = getattr(self, self.config.cfgfile.get("Commands", key))
		self.setEnabled(False)
		
	def reinit(self):
		self.totalcards = []
		self.trash = []
		
		self.play_order = []
		self.active_player = -1
		
		self.running = False
		self.winner = ""
		self.timerecord = 0

		tr = self.data().get_max(self.gameid, "gameduration")
		if not tr is None:
			self.timerecord = tr.gameduration
		self.roundcount = 0
		game.Game.reinit(self)
			
	def register_player(self, nickname, params):
		game.Game.register_player(self, nickname, params)
		if not self.running:
			self.players[nickname] = {
				'ingame': True,
				'cards': [],
				'miles': 0,
				'record': None,
				'canrun': False,
				'limited': False,
				'attack': None,
				'jokers': []
			}
			
			rec = self.data().get_one(self.gameid, gamewinner=nickname)
			if not rec is None:
				self.players[nickname]['record'] = rec.gamewintime
					
	def start_game(self):
		game.Game.start_game(self)
		self.totalcards = []
		
		for c in ATTACKS:
			ln = 3
			if c == 'limite':
				ln = 4
			elif c == 'feu_rouge':
				ln = 5
			for i in range(ln):
				self.totalcards.append(c)
		for c in DEFENSE:
			ln = 6
			if c == 'feu_vert':
				ln = 14
			for i in range(ln):
				self.totalcards.append(c)
		for c in JOKERS:
			self.totalcards.append(c)
		for c in MILES:
			ln = 10
			if c == '100':
				ln = 12
			elif c == '200':
				ln = 4
			for i in range(ln):
				self.totalcards.append(c)
					
		random.shuffle(self.totalcards)
		random.shuffle(self.play_order)
		
		order = '* ORDRE DE JEU : '
		for o in self.play_order:
			order = order + o + ' -> '
		self.emit_pubmsg('\x033 ' + order + '\x03')
		
		self.distribute_cards()
		
		for p in self.players:
			self._show_cards_to_player(p)
			
		self._next_turn(show = False)
		
	def get_a_card(self):
		if len(self.totalcards) == 0:
			for t in self.trash:
				self.totalcards.append(t)
				self.trash.remove(t)
		c = self.totalcards[0]
		self.totalcards.remove(c)
		return c
		
	def distribute_cards(self):
		for p in self.players:
			for i in range(6):
				self.give_card_to_player(p, params = None)
				
	def give_card_to_player(self, player, params):
		newcard = self.get_a_card()
		self.players[player]['cards'].append(newcard)
		return newcard
		
	def trash_card(self, player, params):
		card = self.analyze_card(params[0].lower())
		if card in self.players[player]['cards']:
			self.emit_pubmsg("\x0312 * " + player + " jette une carte " + self._card_to_str(card) + " !\x03")
			self.trash.append(card)
			self.players[player]['cards'].remove(card)
			self._next_turn()
		else:
			self.emit_notice(player, "\x034 * Pourquoi diable voudrais-tu jeter une carte que tu n'as pas ? O_o\x03")
		
	def _card_to_str(self, card, face = False):
		if card in ATTACKS:
			result = '\x034 ' + ATTACKS[card][0] + " (" + ATTACKS[card][1].upper() + ") " + '\x03'
		elif card in DEFENSE:
			result = '\x033 ' + DEFENSE[card][0] + " (" + DEFENSE[card][1].upper() + ") " + '\x03'
		elif card in JOKERS:
			result = '\x032 ' + JOKERS[card][0] + " (" + JOKERS[card][1].upper() + ") " + '\x03'
		else:
			result = '\x0312' + MILES[card] + '\x03'
		return result
		
	def _next_turn(self, show = True):
		nick = self.next_player()
		self.emit_pubmsg('\x02 \x032' + "* C'est au tour de " + nick + " de jouer !" + '\x03\x02')
		self.emit_pubmsg('\x0310 * ' + nick + " a parcouru " + str(self.players[nick]['miles']) + " km.\x03")
		if (self.players[nick]['attack'] is None) and (not self.players[nick]['canrun']):
			self.emit_notice(nick, '\x034 * Il vous faut un FEU VERT pour pouvoir avancer *\x02')
		self.roundcount = self.roundcount + 1
		
		self._display_status(nick)
		
		card = self.give_card_to_player(nick, None)
		self.emit_notice(nick, '\x02\x032' + " * Vous piochez une carte \x03" + self._card_to_str(card) + "\x02")
		if show:
			self._show_cards_to_player(nick)
			
	def _display_status(self, player, params = None):
		if not self.players[player]['attack'] is None:
			self.emit_notice(player, "\x034 * Vous ne pouvez pas avancer à cause de : " + ATTACKS[self.players[player]['attack']][0] + " * \x03")
		if self.players[player]['limited']:
			self.emit_notice(player, "\x034 * Vous êtes limité à 50 km par tour !\x03")

	def show_jokers(self, player, params = None):
		if params and len(params) == 1:
			dest = params[0]
		else:
			dest = player
		self.emit_notice(player, "\x02* Jokers de %s *\x02" % (dest,))
		if len(self.players[dest]['jokers']) > 0:
			for jk in self.players[dest]['jokers']:
				self.emit_notice(player, "\x032  - %s\x03" % (JOKERS[jk][0].upper(),))
		else:
			self.emit_notice(player, "\x034 Aucun joker\x03")

	def show_sabot(self, player, params = None):
		ln = "\x032 -- \x02Sabot\x02 -- Il reste %d cartes à piocher et %d cartes ont été jetées.\x03" % (len(self.totalcards), len(self.trash))
		self.emit_pubmsg(ln)
			
	def _can_play_card(self, player, card, dest):
		if card in MILES and self.players[player]['canrun']:
			if self.players[player]['limited']:
				result = card in ['25', '50']
			else:
				result = True
			if self.players[player]['miles'] + int(card) > 1000:
				result = False
		elif card in JOKERS:
			result = True
		elif card in ATTACKS:
			if not dest is None:
				if (card != 'limite') and ((dest in self.players) and (self.players[dest]['attack'] is None) and (self.players[dest]['canrun'])):
					result = True
				elif card == 'limite' and not self.players[dest]['limited']:
					result = True
				elif not dest in self.players:
					self.emit_notice(player, "\x034 * Le joueur " + dest + " n'existe pas ! :o\x03")
					result = False
				else:
					result = False
			else:
				result = False
		elif card in DEFENSE:
			if self.players[player]['attack'] == 'panne' and card == 'essence':
				result = True
			elif self.players[player]['attack'] == 'accident' and card == 'repare':
				result = True
			elif self.players[player]['attack'] == 'crevaison' and card == 'roue_secours':
				result = True
			elif (not self.players[player]['canrun']) and (card == 'feu_vert') and ((self.players[player]['attack'] is None) or (self.players[player]['attack'] == 'feu_rouge')):
				result = True
			elif self.players[player]['limited'] and card == 'fin_limite':
				result = True
			else:
				result = False
		else:
			result = False
		return result
			
	def __add_score_to_player(self, player, score):
		self.players[player]['miles'] = self.players[player]['miles'] + score
		
	def __attack_player(self, player, dest, card):
		if player == dest:
			self.emit_pubmsg("\x0312 * On dirait que " + player + " est devenu schizophrène... il veut s'attaquer lui-même !! \x03")
		else:
			if card == 'accident' and 'as_volant' in self.players[dest]['jokers']:
				self.emit_pubmsg("\x035 * " + dest + " ne peut pas avoir d'accident ! C'est un AS DU VOLANT ! :P\x03")
			elif card == 'panne' and 'citerne' in self.players[dest]['jokers']:
				self.emit_pubmsg("\x035 * " + dest + " a une CITERNE D'ESSENCE ! Plus de problème de carburant :P\x03")
			elif card == 'crevaison' and 'increvable' in self.players[dest]['jokers']:
				self.emit_pubmsg("\x035 * " + dest + " a des roues INCREVABLES ! Range tes clous ça sert à rien ^^\x03")
			elif card in ['feu_rouge', 'limite'] and 'prioritaire' in self.players[dest]['jokers']:
				self.emit_pubmsg("\x035 * " + dest + " allume son girophare et sa sirène ! Il est dans un VEHICULE PRIORITAIRE !\x03")
			else:
				if card != 'limite' and self.players[dest]['canrun']:
					self.players[dest]['attack'] = card
					self.players[dest]['canrun'] = False
				else:
					self.players[dest]['limited'] = True
		
	def __play_defense_card(self, player, card):
		_good_card = False
		if card == 'fin_limite':
			if self.players[player]['limited']:
				self.emit_pubmsg('\x02\x033 * ' + player + " quitte la zone de limitation de vitesse !\x03\x02")
				self.players[player]['limited'] = False
			else:
				self.emit_pubmsg("\x02\x034 * Je sais que tu veux aller vite, " + player + ", mais là... t'es pas limité...\x03\x02")
		else:
			if self.players[player]['attack'] == 'panne':
				if card == 'essence':
					self.emit_pubmsg('\x02\x033 * ' + player + " est allé chercher un jerrycan d'essence à la station " + REFUEL_NAMES[random.randint(0, len(REFUEL_NAMES)-1)] + " la plus proche !\x03\x02")			
					self.players[player]['canrun'] = False
					_good_card = True
				else:
					self.emit_pubmsg('\x02\x034 * Bon... tu es irrécupérable... parce que jouer ' + DEFENSE[card][0] + " sur une panne d'essence, faut être un peu bêta non ?!" + '\x03\x02')
			elif self.players[player]['attack'] == 'accident':
				if card == 'repare':
					self.emit_pubmsg('\x02\x033 * ' + player + " vient de récupérer son véhicule à la sortie du garage :D\x03\x02")
					self.players[player]['canrun'] = False
					_good_card = True
				else:
					self.emit_pubmsg('\x02\x034' + " * Tes capacités sont probablement diminuées par la fatigue... jouer " + DEFENSE[card][0] + " sur une voiture cassée... c'est limite acceptable..." + '\x03\x02')
			elif self.players[player]['attack'] == 'crevaison':
				if card == 'roue_secours':
					self.emit_pubmsg('\x02\x033 * ' + player + ", au prix d'un effort surhumain, a réussi à changer sa roue \\o/\x03\x02")
					self.players[player]['canrun'] = False
					_good_card = True
				else:
					self.emit_pubmsg('\x02\x034 * Alors je sais que je ne suis pas très malin... mais jouer une carte ' + DEFENSE[card][0] + ' sur un pneu crevé je doute que ça fonctionne...' + '\x03\x02')
			elif not self.players[player]['canrun'] and card == 'feu_vert':
				self.emit_pubmsg('\x02\x033 * ' + player + " peut à présent rouler !!! \x03\x02")
				self.players[player]['canrun'] = True
				_good_card = True
			
		self.road_for_priority(player, card)
		if _good_card:
			self.players[player]['attack'] = None
		
	def check_winner(self, player):
		if self.players[player]['miles'] == 1000:
			self.winner = player
			if self.total_time < self.timerecord:
				self.emit_pubmsg('\x02\x0312 * ' + player + " vient d'exploser le Record Absolu ! Félicitations !!" + '\x03\x02')
			elif self.players[player]['record'] is None or self.total_time < self.players[player]['record']:
				self.emit_pubmsg('\x02\x033 * ' + player + " bat son record de vitesse de victoire \\o/" + '\x03\x02')
			del self.players[player]
			self.play_order.remove(player)
			self.emit_pubmsg('\x02\x0310 * ' + player + " gagne la partie en " + util.seconds_to_string_time(self.total_time) + " ! *" + '\x03\x02')
			self.gamestatitem.gamewinner = player
			self.gamestatitem.gamewintime = self.total_time
			self.emit_pubmsg('\x02 ** PARTIE TERMINEE ** TEMPS TOTAL: ' + util.seconds_to_string_time(self.total_time) + ' (' + str(self.roundcount) + " tours de jeu)" + ' **\x02')
			if self.total_time < self.timerecord:
				self.emit_pubmsg("\x02\x0312 * Cette partie est la plus rapide de l'histoire du jeu ! Waouh O_O ! \x03\x02")
			self.emit_pubmsg('\x02\x033 ' + self.winner + ' remporte cette partie ! Félicitations :) ! ' + '\x03\x02')
			self.gamestatitem.gameduration = self.total_time
			self.gamestatitem.channel = self.param('channel')
			self.gamestatitem.gamedate = datetime.now().strftime("%d/%m/%Y")
			try:
				self.save_statitem()
			except Exception as e:
				print(str(e))
			self.stop_game()
		else:
			self._next_turn()
			
	def road_for_priority(self, player, card):
		if 'prioritaire' in self.players[player]['jokers'] and card != 'feu_vert' and card != 'prioritaire':
			self.players[player]['canrun'] = True
			self.emit_pubmsg("\x02\x033 * Grâce au VEHICULE PRIORITAIRE, " + player + " peut rouler dès maintenant ! \x03\x02")
			
	def __play_joker(self, player, card):
		self.players[player]['jokers'].append(card)
		if card == 'prioritaire':
			if (self.players[player]['attack'] is None) or (self.players[player]['attack'] == 'feu_rouge') or (self.players[player]['limited']):
				if self.players[player]['attack'] == 'feu_rouge':
					self.players[player]['attack'] = None
				self.players[player]['limited'] = False
				self.players[player]['canrun'] = True
				self.emit_notice(player, "\x033 * Vous pouvez rouler !\x03")
		elif card == 'citerne':
			if (self.players[player]['attack'] == 'panne'):
				self.emit_pubmsg("\x02\x033 * " + player + " achète une citerne de 500 000 litres d'essence à Nasser... Finies les pannes sèches ! \x03\x02")
				self.players[player]['attack'] = None
		elif card == 'as_volant':
			if (self.players[player]['attack'] == 'accident'):
				self.emit_pubmsg("\x02\x033 * " + player + " appelle La Sorcière Bien-Aimée qui lui répare son véhicule et devient indestructible !! \x03\x02")
				self.players[player]['attack'] = None
		elif card == 'increvable':
			if (self.players[player]['attack'] == 'crevaison'):
				self.emit_pubmsg("\x02\x033 * " + player + " fait monter des pneus increvables... Même les clous se cassent la tête dessus :o ! \x03\x02")
				self.players[player]['attack'] = None
				
		self.road_for_priority(player, card)
		
	def play_card(self, player, params):
		bad = False
		if player.lower() == self.play_order[self.active_player].lower():
			card = self.analyze_card(params[0].lower())
			dest = None
			if card in ATTACKS:
				if len(params) == 1:
					self.emit_notice(player, "* Hé banane! Pour lancer une attaque, faut dire sur qui tu veux la lancer -_-...")
					bad = True
				else:
					dest = params[1]
					bad = False
			
			if not bad:
				if card in self.players[player]['cards']:
					if self._can_play_card(player, card, dest):
						if card in MILES:
							self.emit_pubmsg('\x0312 * ' + player + " avance de " + MILES[card] + " !\x03")
							self.__add_score_to_player(player, int(card))
						elif card in JOKERS:
							self.emit_pubmsg('\x032 * ' + player + " joue le joker " + JOKERS[card][0].upper() + " !\x03")
							self.__play_joker(player, card)
						elif card in ATTACKS:
							self.emit_pubmsg('\x034 * ' + player + " lance l'attaque " + ATTACKS[card][0] + " sur " + dest + " !\x03")
							self.__attack_player(player, dest, card)
						elif card in DEFENSE:
							self.emit_pubmsg('\x033 * ' + player + " joue la carte " + DEFENSE[card][0] + " !\x03")
							self.__play_defense_card(player, card)
							
						self.players[player]['cards'].remove(card)
						self.check_winner(player)
					else:
						self.emit_notice(player, "\x034* Tu ne peux pas jouer cette carte !\x03")
				else:
					self.emit_notice(player, "\x033* Oh petit malin, t'essaies de jouer une carte que tu n'as pas !! :o Franchement... tu devrais avoir honte ! :3\x03")
		else:
			self.emit_notice(player, "* Ce n'est pas encore ton tour de jouer ! Sois patient petit scarabée ^^")		
			
	def _show_cards_to_player(self, player):
		cds = self.players[player]['cards']
		s = "* Vos cartes -- "
		for c in cds:
			s = s + self._card_to_str(c) + " - "
		self.emit_notice(player, s)
			
	def get_help(self, player, params):
		self.emit_privmsg(player, "\x02 * Aide du jeu Mille Bornes *\x02")
		self.emit_privmsg(player, "\x0310 Le but du jeu du 1000 Bornes est de parcourir 1000 kilomètres avec les cartes du jeu.\x03")
		self.emit_privmsg(player, "\x0310 4 catégories de cartes :\x03")
		self.emit_privmsg(player, "\x0310  - Les bornes : 25, 50, 75, 100 et 200 km.\x03")
		self.emit_privmsg(player, "\x0310  - Les bottes : Véhicule Prioritaire, Citerne d'essence, Increvable et As du Volant.\x03")
		self.emit_privmsg(player, "\x0310  - Les attaques : Crevaison, Panne d'essence, Feu Rouge, Limitation de vitesse, et Accident.\x03")
		self.emit_privmsg(player, "\x0310  - Les parades : Roue de Secours, Essence, Feu Vert, Fin de limitation, et Réparations.\x03")
		self.emit_privmsg(player, "\x02\x033 * Commandes disponibles \x03\x02")
		self.emit_privmsg(player, "\x0310  - !play : S'inscrire pour la prochaine partie\x03")
		self.emit_privmsg(player, "\x0310  - !unplay: Se désinscrire de la prochaine partie\x03")
		self.emit_privmsg(player, "\x0310  - !card <code_carte>: Jouer une carte. Le code carte est affiché entre parenthèses à côté de la carte concernée avec deux lettres. Pour les bornes, il suffit de saisir le nombre de km de la carte.\x03")
		self.emit_privmsg(player, "\x0310  - !trash <code_carte>: Se défausser d'une carte. Le code carte est le même que pour !card.\x03")
		self.emit_privmsg(player, "\x0310 La partie s'arrête lorsqu'un des joueurs a atteint les 1000 bornes.\x03")
		self.emit_privmsg(player, "\x0310 En raison du nombre conséquent de notices envoyées, merci de fermer ce dialogue privé :) Bonne chance ! :)\x03")
		
	def show_mycards(self, player, params):
		self._display_status(player)
		self._show_cards_to_player(player)
				
	def analyze_card(self, c):
		card = c.lower()
		if card == 'fv':
			return 'feu_vert'
		elif card == 'fr':
			return 'feu_rouge'
		elif card == 'rs':
			return 'roue_secours'
		elif card == 'fl':
			return 'fin_limite'
		elif card == 'av':
			return 'as_volant'
		elif card == 'vp':
			return 'prioritaire'
		elif card == 'ce':
			return 'citerne'
		elif card == 'pe':
			return 'panne'
		elif card == 'ac':
			return 'accident'
		elif card == 'cv':
			return 'crevaison'
		elif card == 'lv':
			return 'limite'
		elif card == 'es':
			return 'essence'
		elif card == 'rp':
			return 'repare'
		elif card == 'iv':
			return 'increvable'
		elif card[:2] in ['25', '50', '75']:
			return card[:2]
		elif card [:3] in ['100', '200']:
			return card[:3]
		else:
			return card
