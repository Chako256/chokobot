#!/usr/bin/env python3
#-*- coding:utf-8 -*-

from mybot import MyBot
import sys
from argparse import ArgumentParser


class ChokoBot:
	"""
	ChokoBot

	@version 6.0
	@dependencies MyBot~=5.x
	"""
	def __init__(self):
		self.core = MyBot(
			debug_level="DEBUG",
			fmt="yaml"
		)

	def start(self):
		try:
			self.core.start()
		except Exception as ex:
			raise ex
			sys.exit(1)

def start_bot():
	ChokoBot().start()

if __name__ == '__main__':
	parser = ArgumentParser(description="ChokoBot v6.0 - IRC Robot")
	parser.add_argument('--daemonize', action='store_true', help='Daemonize the robot. There is no TTY log.')

	args = parser.parse_args()
	start_bot()
